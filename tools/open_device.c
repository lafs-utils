
/* support routine to open device or file for lafs tools. */

#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "internal.h"

int open_device(char *devname, long long *device_bytes, int regular_file,
		int force, char **error)
{
	/* must be able to get an exclusive open on the device and its size
	 * must be non-trivial
	 * If 'regular_file', then expect a regular file to be used instead.
	 * If device_bytes is already non-zero then it is OK to create
	 * a regular file.
	 */
	int fd;
	struct stat stb;
	unsigned long long size = 0;

	*error = NULL;

	if (!regular_file)
		fd = open(devname, O_RDWR|(force?0:O_EXCL));
	else if (*device_bytes)
		fd = open(devname, O_RDWR|O_CREAT, 0666);
	else
		fd = open(devname, O_RDWR);
	if (fd < 0 || fstat(fd, &stb) < 0) {
		asprintf(error, "cannot open %s %s: %s",
			 regular_file? "file" : "device",
			 devname, strerror(errno));
		return -1;
	}

	if (regular_file) {
		if ((stb.st_mode & S_IFMT) != S_IFREG)
			asprintf(error, "%s is not a regular file",
				devname);
		else {
			if (*device_bytes) {
				char zero = 0;
				lseek64(fd, *device_bytes-1, 0);
				write(fd, &zero, 1);
				fstat(fd, &stb);
			}
			size = stb.st_size;
		}
	} else {
		if ((stb.st_mode & S_IFMT) != S_IFBLK)
			asprintf(error, "%s is not a block device",
				devname);
		else if (ioctl(fd, BLKGETSIZE64, &size) != 0)
			asprintf(error, "Cannot get size of %s",
				devname);
	}

	if (!error && size < 64*1024)
		asprintf(error, "%s is too small for a LAFS filesystem",
			devname);

	if (*error) {
		close(fd);
		return -1;
	}
	*device_bytes = size;
	lseek64(fd, 0, 0);
	return fd;
}
