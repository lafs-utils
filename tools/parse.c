
/* Simple number parsing code */

#include <stdlib.h>
#include "internal.h"

long long parse_size(char *arg, char **error)
{
	long long val;
	long long scale = 1;
	char *endp;

	val = strtoll(arg, &endp, 0);

	if (endp == arg) {
		*error = "not a valid number";
		return 0;
	}

	if (endp[0] && endp[1]) {
		*error = "unrecognised modifier (need K,M,G)";
		return 0;
	}
	switch(*endp) {
	case 'k':
	case 'K':
		scale = 1024;
		break;
	case 'M':
		scale = 1024*1024;
		break;
	case 'G':
		scale = 1024*1024*1024;
		break;
	case '\0':
		scale = 1;
		break;
	default:
		*error = "unrecognised modifier (need K,M,G)";
		return 0;
	}
	if (val == 0) {
		*error = "zero is not valid";
		return 0;
	}
	val *= scale;
	error = NULL;
	return val;
}
