/*
 * lafs - Examine and manipulate and LaFS image
 *
 *  This program is essentially a front-end to liblafs.  It allows
 *  a LaFS filesystem to be examined and modified.  All interesting
 *  manipulations are function calls in to liblafs.
 *  The program simply parses textual commands converting them into
 *  library calls, and provides a readline interface with autocompletion
 *  and context-sensitive help.
 *
 * Copyright (C) 2011 NeilBrown <neil@brown.name>
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    Author: Neil Brown
 *    Email: <neil@brown.name>
 *
 */

#define _GNU_SOURCE
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <stdio.h>
#include <errno.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <lafs/lafs.h>
#include <talloc.h>
#include <uuid/uuid.h>
#include "internal.h"

/* This is the global state which is passed around among
 * the various commands.
 */
struct state {
	struct lafs *lafs;
	int done;
	int verbose;
};

static struct state *gen_state;

/* Every command can have arguments, both positional and
 * named.
 * Named arguments are specified with a leading hyphen, though
 * multiple hyphens may be given in input (completion chooses 2).
 * Tags for positional arguments must not start with '-', and is used
 * only for help messages.
 * Named arguments are normally followed by an '=' and a value.  If an argument is
 * defined as a 'flag' then no '=' is expected.
 * A tag argument can provide a value to a positional argument, and the
 * command can easily determine if the tag version was used.
 * Each tag may only be given once.
 * A positional argument can be a subcommand which links to a new set
 * of argument descriptions.
 * types are:
 *   flag:  expect --tagname      This is either present or not.
 *   opaque:                      An uninterpreted string, often a number.
 *   choice:			  On of a defined list of strings.
 *   external:                    An external filename - completion is possible.
 *   internal:                    An internal filename - completion might be possible.
 *   subcommand:                  one of a list of subcommands.
 * Any unique prefix of a tag is allowed to match.
 */
enum argtype { flag, opaque, choice, external, internal, subcommand};
static struct args {
	char *tag;
	enum argtype type;
	int pos;
	union { struct cmd *subcmd; char **options; };
	char *desc;
} lafs_args[];
#define TERMINAL_ARG {NULL, opaque, 0, {NULL}, NULL}

/* When a positional parameter is identified as 'subcommand' it is associated
 * with a list of 'struct cmd' identifying the possible subcommands.
 * Any unique prefix of a command is allowed to match.
 * The initial argument list description allows a single argument which is
 * a 'subcommand' identifying all the commands that lafs understands.
 */
static struct cmd {
	char *name;
	void (*cmd)(struct state *st, void **args);
	struct args *args;
	char *help;
} lafs_cmds[];

/* Find a command in a list.  The word must be an exact match, or
 * a unique prefix.
 */
static struct cmd *find_cmd(struct cmd *cmds, char *word)
{
	int c;
	int l = strlen(word);
	int best = -1;

	for (c = 0; cmds[c].name; c++) {
		if (strcmp(word, cmds[c].name) == 0)
			return cmds+c;
		if (strncmp(word, cmds[c].name, l) == 0) {
			if (best == -1)
				best = c;
			else
				best = -2;
		}
	}
	if (best < 0)
		return NULL;
	return cmds + best;
}

/* Find a tag in an argument list.  The given tag (up to the given length)
 * must be an exact match or a unique prefix.
 */
static int find_tag(struct args *args, const char *tag, int len)
{
	int i;
	int best = -1;

	for (i = 0; args[i].tag; i++)
		if (args[i].tag[0] == '-') {
			if (strncmp(tag, args[i].tag+1, len) != 0)
				continue;
			if (strlen(args[i].tag+1) == len)
				return i;
			if (best == -1)
				best = i;
			else
				best = -2;
		}
	return best;
}

/* Find an option in the list, return position.
 * No prefixing is allowed
 */
static int find_option(char **options, char *w)
{
	int i;
	for (i = 0; options[i]; i++)
		if (strcmp(options[i], w) == 0)
			return i;
	return -1;
}

/* Return the first word on the line, modifying the line in-place and
 * updating *line to be ready to get the next word.
 *
 * Space/tab separates words.  ' or " quotes words.
 * \ protects quotes and spaces.
 */
static char *take_word(char **line)
{
	char *rv = *line; /* the buffer we will return - if non-empty */
	char *lp = rv;    /* the line we are examining */
	char *wp = rv;    /* the end of the word we are creating. */
	static char *delim = " \t\n";
	char quote = '\0';

	while (*lp && strchr(delim, *lp) != NULL)
		lp++;

	while (*lp && (quote || strchr(delim, *lp) == NULL)) {
		if (quote && *lp == quote) {
			lp++;
			continue;
		}
		switch(*lp) {
		case '\'':
		case '"':
			if (quote == *lp) {
				quote = '\0';
				continue;
			}
			if (!quote) {
				quote = *lp++;
				continue;
			}
			break;
		case '\\':
			if (lp[1] == '\'' || lp[1] == '"' || lp[1] == ' ')
				lp++;
			break;
		}
		*wp++ = *lp++;
	}
	if (*lp)
		lp++;
	*line = lp;
	*wp = '\0';
	if (wp > rv)
		return rv;
	return NULL;
}

static int get_int(char *str, int *result)
{
	long int num;
	char *ep;

	num = strtol(str, &ep, 10);
	if (*str && !*ep) {
		*result = num;
		return 0;
	}
	return -1;
}

/* Return an array of void* corresponding to the entries in
 * 'args'.  If an arg is present, the array entry is not NULL.
 * For flags, the array entry will be the full flag.
 * for --tag=, the array entry will be from after the '='
 * for positional, the array entry will be the whole arg.
 * where a tag can fill a positional, the value is placed in both
 * slots.
 * If an arg is a subcommand, then the 'struct cmd' point is placed
 * in the return array rather than a string.
 * The 'args' pointer is updated to the most precise subcommand.
 * '*offsetp' is set to the offset in the returned array of the 
 * first argument in the new 'args' list.
 * '*lastp' is set to the last tag in 'args' that was matched.
 * '*error' is an error message if something when astray.
 *
 * Named arguments can appear before, after, or among positional
 * arguments.
 */
static void **parse_line(struct args **argsp, char *line, int *offsetp,
			 int *lastp, char **error)
{
	void **rv;
	int i;
	char *w;
	int offset = 0;
	int size;
	struct args *args = *argsp;

	if (lastp)
		*lastp = -1;

	for (i = 0; args[i].tag; i++)
		;
	rv = calloc(i+offset, sizeof(char*));
	size = i+offset;

	while (!*error && (w = take_word(&line)) != NULL) {
		if (*w == '-') {
			/* Find the matching tag. */
			char *t = w, *e;
			int n, n2;
			while (*t == '-')
				t++;
			e = t;
			while (*e && *e != '=')
				e++;
			n = n2 = find_tag(args, t, e-t);
			if (n < 0) {
				asprintf(error, "Unrecognised option: %s", w);
				break;
			}
			if (lastp)
				*lastp = n;
			if (args[n].pos >= 0)
				n2 = args[n].pos;
			if (rv[n+offset] != NULL || rv[n2+offset] != NULL) {
				asprintf(error, "Duplicate option: %s", w);
				break;
			} else {
				if (*e == '=')
					w = e+1;
				else if (args[n].type != flag) {
					w = take_word(&line);
					if (!w) {
						asprintf(error,
							 "Missing value for --%s", t);
						break;
					}
				}
				rv[n+offset] = w;
				rv[n2+offset] = w;
			}
		} else {
			/* must be next positional */
			for (i=0;
			     args[i].tag && args[i].tag[0] != '-';
			     i++)
				if (rv[i+offset] == NULL)
					break;
			if (args[i].tag == NULL || args[i].tag[0] == '-') {
				/* No positions left */
				asprintf(error, "Extra positional parameter: %s", w);
				break;
			}
			rv[i+offset] = w;
			/* If this is a subcommand arg then we need to
			 * parse the remaining args in the context of the
			 * given subcommand - if it exists.
			 */
			switch(args[i].type) {
				struct cmd *c;
				int o;
			default: break;
			case choice:
				o = find_option(args[i].options, w);
				if (o < 0)
					asprintf(error, "Value %s for %s is not acceptable", w, args[i].tag);
				break;
			case subcommand:
				c = find_cmd(args[i].subcmd, w);
				rv[i+offset] = c;
				if (c) {
					args = c->args;
					*argsp = args;
					offset += i+1;
					if (lastp)
						*lastp = -1;
					for (i = 0; args[i].tag; i++)
						;
					rv = realloc(rv, (i+offset) * sizeof(void*));
					while (size < i + offset) {
						rv[size] = NULL;
						size++;
					}
				} else
					asprintf(error, "Unrecognised command: %s",w);
				break;
			}
		}
	}
	if (offsetp)
		*offsetp = offset;
	return rv;
}

/* parse and execute the given command line against the given state. */
static int execute_line(struct state *st, char *line)
{
	struct cmd *c;
	struct args *args = lafs_args;
	void **arglist;
	char *error = NULL;

	arglist = parse_line(&args, line, NULL, NULL, &error);
	
	if (error) {
		fprintf(stderr, "lafs: %s\n", error);
		free(error);
		free(arglist);
		return -1;
	}
	c = (struct cmd*)arglist[0];
	if (c)
		c->cmd(st, arglist);
	free(arglist);
	return 1;
}

static char **complete_in_context(const char *word, int start, int end);

/* 'interact' is the main interface when used interactively.
 * It reads lines using 'readline' and executes them.
 * 'readline' is configured to provide context sensitive completion
 * and help.
 */
static void interact(struct state *st)
{
	char *home, *hist;
	rl_attempted_completion_function = complete_in_context;
	rl_basic_word_break_characters = " \t\n=";
	rl_completer_quote_characters = "\"'";
	rl_initialize();

	home = getenv("HOME");
	if (!home)
		home = ".";
	asprintf(&hist, "%s/.lafs_history", home);
	read_history(hist);
	
	gen_state = st;
	while (!st->done) {
		char *line = readline("LaFS: ");

		if (!line) {
			printf("\n");
			break;
		}

		if (*line)
			add_history(line);
		execute_line(st, line);

		free(line);
	}
	write_history(hist);
}

/* 'runfile' is the alternate interface when a regular file is
 * given with commands.  It reads and executes commands until
 * it finds an error.
 */
static void runfile(struct state *st, FILE *f)
{

	while (!st->done) {
		char *line = NULL;
		ssize_t len;
		size_t size;

		len = getline(&line, &size, f);

		if (len <= 0)
			st->done = 1;
		else if (execute_line(st, line) < 0)
			st->done = 1;

		free(line);
	}
}


int main(int argc, char *argv[])
{
	struct state st = {0};
	st.lafs = lafs_alloc();
	if (argc > 1) {
		if (strcmp(argv[1], "-") == 0)
			runfile(&st, stdin);
		else {
			FILE *f = fopen(argv[1], "r");
			if (f) {
				runfile(&st, f);
				fclose(f);
			} else
				fprintf(stderr, "lafs: cannot open %s\n", argv[1]);
		}
	} else {
		st.verbose = 1;
		interact(&st);
	}
	exit(0);
}

/*
 * Here be routines to provide context sensitive completion and
 * help.
 * Completion understands:
 *  - lists of subcommands
 *  - lists of tags for options
 *  - options that expect filenames, whether internal or external.
 * Before listing possible matches, the description of the expected
 * option is given.
 */

/* cmd_gen is used to generate a list of matching commands.
 * 'gen_cmds' must be initialised to point to the list.
 */
static struct cmd *gen_cmds;
static char *cmd_gen(const char *prefix, int state)
{
	static int next;
	int len = strlen(prefix);
	if (state == 0)
		next = 0;
	for ( ; gen_cmds[next].name ; next++)
		if (strncmp(prefix, gen_cmds[next].name, len) == 0) {
			next++;
			return strdup(gen_cmds[next-1].name);
		}
	return NULL;
}

/* tag_gen is used to generate a list of expected tags.
 * 'gen_args' is the relevant argument list.
 * 'gen_found' is used to determine which tags have already been given,
 *    so they are not offered again.
 * generated tags always start "--" even if user types "-ta".
 */
static struct args *gen_args;
static void **gen_found;
static char *tag_gen(const char *prefix, int state)
{
	static int next;
	int len;

	if (state == 0)
		next = 0;

	while (*prefix == '-')
		prefix++;
	len = strlen(prefix);

	for ( ; gen_args[next].tag; next++) {
		char *c;
		if (gen_args[next].tag[0] != '-')
			continue;
		if (gen_found[next])
			continue;
		if (gen_args[next].pos >= 0 &&
		    gen_found[gen_args[next].pos])
			continue;
		if (strncmp(prefix, gen_args[next].tag+1, len) != 0)
			continue;

		c = malloc(2 + strlen(gen_args[next].tag+1) + 2);
		strcpy(c, "--");
		strcpy(c+2, gen_args[next].tag+1);
		if (gen_args[next].type != flag) {
			strcat(c, "=");
			rl_completion_suppress_append = 1;
		}
		next++;
		return c;
	}
	return NULL;
}

/* choice_gen is used to generate a list of possible values
 * for a 'choice' field.
 * 'gen_options' is the options that can go here.
 */
static char **gen_options;
static char *choice_gen(const char *prefix, int state)
{
	static int next;
	int len;

	if (state == 0)
		next = 0;

	len = strlen(prefix);
	for (; gen_options[next] ; next++) {
		if (strncmp(prefix, gen_options[next], len) != 0)
			continue;
		next++;
		return strdup(gen_options[next-1]);
	}
	return NULL;
}

static char *internal_gen(const char *prefix, int state)
{
	static char *cp;
	static char *pre = NULL;
	static struct lafs_ino *inode;
	static u32 index;

	u32 inum;
	int type;
	char name[257];

	if (gen_state->lafs->ss.root == 0) {
		rl_attempted_completion_over = 1;
		return NULL;
	}
	if (state == 0) {
		if (pre)
			free(pre);
		pre = strdup(prefix);
		cp = pre + strlen(pre);
		while (cp > pre && cp[-1] != '/')
			cp--;
		prefix = cp;
		while (cp > pre && cp[-1] == '/')
			cp--;
		cp[0] = 0;
		/*pre is the directory, prefix is the component prefix */
		inode = lafs_get_itable(gen_state->lafs);
		inode = lafs_get_inode(inode, 2);
		inode = lafs_lookup_path(inode, inode, pre, NULL);
		index = -1;
	} else {
		if (index + 1 == 0) {
			rl_attempted_completion_over = 1;
			return NULL;
		}
	}
	if (lafs_dir_next(inode, &index, name, &inum, &type) == 1)
		return strdup(name);

	rl_attempted_completion_over = 1;
	return NULL;
}
/*
 * This is the brains of the completion handler.
 * We parse the line-so-far to determine way options have already
 * been provided, and so what is left to provide.
 * We then look at the given prefix to determine if a positional or
 * tag argument is most likely, and provide relevant completions.
 */
static char **complete_in_context(const char *prefix, int start, int end)
{
	static char *buf = NULL;
	static int bufsize = 0;

	char *line;
	void **arglist;
	struct args *args;
	int offset, last;
	int p;
	char *error = NULL;
	char **matches = NULL;

	while (bufsize < start+1) {
		bufsize += 80;
		buf = realloc(buf, bufsize);
	}
	memcpy(buf, rl_line_buffer, start);
	buf[start] = 0;
	line = buf;

	args = lafs_args;
	arglist = parse_line(&args, line, &offset, &last, &error);

	if (last >= 0 &&
	    error && strncmp(error, "Missing value for", 17) == 0) {
		free(error);
		error = 0;
	} else if (!(start && rl_line_buffer[start-1] == '='))
		last = -1;

	if (error) {
		printf("\n *** %s ***\n", error);
		free(error);
		goto after_message;
	}

	if (last >= 0 && (arglist[last+offset] == NULL ||
			  ((char*)arglist[last+offset])[0] == '\0'))
		p = last;
	else {
		last = -1;
		for (p = 0; args[p].tag && args[p].tag[0] != '-' ; p++)
			if (arglist[p+offset] == NULL)
				break;
		if (args[p].tag == NULL || args[p].tag[0] == '-')
			p = -1;
	}
	/* 'p' is the arg we expect here, either first positional arg that
	 *  we haven't seen, or tag that we have "--tag=" for. */

	if (last >= 0 || (p >= 0 && (!*prefix || *prefix != '-'))) {
		switch(args[p].type) {
		case subcommand:
			gen_cmds = args[p].subcmd;
			matches = rl_completion_matches(prefix, cmd_gen);
			break;
		case external:
			matches = rl_completion_matches(
				prefix,	rl_filename_completion_function);
			break;
		case internal:
			matches = rl_completion_matches(prefix, internal_gen);
			break;
		case choice:
			gen_options = args[p].options;
			matches = rl_completion_matches(
				prefix, choice_gen);
			break;
		default:
			break;
		}
		if (rl_completion_type == '?') {
			printf("\n *** Please give: %s ***", args[p].desc);
			if (!matches) {
				printf("\n");
				rl_on_new_line();
			}
		}
		rl_attempted_completion_over = 1;
		return matches;
	}
	if (!*prefix || *prefix == '-') {
		gen_args = args;
		gen_found = arglist + offset;
		rl_attempted_completion_over = 1;
		return rl_completion_matches(prefix, tag_gen);
	}

	printf("\n *** No further positional arguments expected:"
	       " try '-' instead ***\n");
after_message:
	rl_on_new_line();
	rl_attempted_completion_over = 1;
	return NULL;
}

/***********************************************************************8
 * Here are the commands.
 * Each command X must define
 *   static char help_X = "One line of description for the command";

 *   static struct args args_X[] = { list of arg definitions; TERMINAL_ARG};
 *   static void c_X(struct state *st, void *args) { implement command ;}
 * and must be listed in lafs_cmds below.
 */

/* common helper functions... */
static long long parse_size_print(char *arg, char **error, char *func, char *name)
{
	long long rv = parse_size(arg, error);
	if (*error)
		printf("%s: %s: %s for %s\n", func, *error, arg, name);
	return rv;
}
static long parse_num_print(char *arg, char **error, char *func, char *name)
{
	char *endp;
	long rv = strtol(arg, &endp, 0);
	if (!*arg || *endp) {
		*error = "Not a valid number";
		printf("%s: %s: %s for %s\n", func, *error, arg, name);
	}
	return rv;
}

static struct lafs_ino *get_inode(struct lafs *fs, void *args[], char **err)
{
	struct lafs_ino *inode;
	*err = NULL;
	if (fs->ss.root == 0) {
		asprintf(err, "filesystem not ready for inode access");
		return NULL;
	}
	inode = lafs_get_itable(fs);
	if (args[1]) {
		char *inostr = args[1];
		char *endp;
		int ino = strtoul(inostr, &endp, 10);
		if (endp == inostr || *endp) {
			asprintf(err, "%s is not a valid inode number",
				 inostr);
			return NULL;
		}
		if (ino)
			inode = lafs_get_inode(inode, ino);
		if (!inode) {
			asprintf(err, "cannot find inode %d",ino);
			return NULL;
		}
	} else {
		char *path = args[0];
		inode = lafs_get_inode(inode, 2);
		inode = lafs_lookup_path(inode, inode, path, NULL);

		if (!inode) {
			asprintf(err, "cannot find inode for %s", path);
			return NULL;
		}
	}
	return inode;
}

/****** EXIT ******/
static char help_exit[] = "Exit lafs";
static struct args args_exit[] = { TERMINAL_ARG };
static void c_exit(struct state *st, void **args)
{
	st->done = 1;
}
/****** QUIT ******/
#define help_quit help_exit
#define c_quit c_exit
#define args_quit args_exit

/****** HELP ******/
static char help_help[] = "Print help for a command or all commands";
static struct args args_help[] = {
	{ "COMMAND", subcommand, -1, {lafs_cmds}, "Command to display help for"},
	{ "-all", flag,      -1, {NULL}, "List brief help for all commands"},
	TERMINAL_ARG
};

static void c_help(struct state *st, void **args)
{
	int c;
	struct cmd *cmd = args[1];

	if (cmd == NULL && args[2] == NULL) {
		for (c = 0; lafs_cmds[c].name; c++) {
			printf("%-9s ", lafs_cmds[c].name);
			if ((c%8)==7)
				printf("\n");
		}
		printf("\n");
		return;
	}

	if (cmd) {
		printf("%s: %s\n", cmd->name, cmd->help);
		if (cmd->args[0].tag) {
			int i;
			printf(" Usage: %s", cmd->name);
			for (i=0; cmd->args[i].tag; i++) {
				struct args *a = cmd->args+i;
				if (a->tag[0] == '-') {
					printf(" [--options...]");
					break;
				}
				printf(" %s", a->tag);
			}
			printf("\n");
			printf(" Arguments:\n");
			for (i=0; cmd->args[i].tag; i++) {
				struct args *a = cmd->args+i;
				if (a->tag[0] != '-') {
					printf("  %-15s: %s\n", a->tag, a->desc);
				} else
					printf("  -%-14s: %s\n", a->tag, a->desc);
			}
		}
	} else {
		printf("-------------------------------------------\n");
		for (c=0; lafs_cmds[c].name; c++)
			printf("%-9s: %s\n", lafs_cmds[c].name, lafs_cmds[c].help);
	}
}

/****** RESET ******/
static char help_reset[] = "Forget all fs information, and prepare to start afresh";
static struct args args_reset[] = { 
	{ "-force", flag, -1, {NULL}, "Reset even if there are unflushed changes"},
	TERMINAL_ARG
};
static void c_reset(struct state *st, void **args)
{

	if (st->lafs->blocksize == 0) {
		printf("reset: Filesystem state is already clear\n");
		return;
	}

	if (args[1] == NULL &&
	    !list_empty(&st->lafs->wc[0].blocks)) {
		printf("reset: filesystem has unflushed changes.  Consider using\n"
		       "       \"flush\" command of \"--force\" argument\n");
		return;
	}
	talloc_free(st->lafs);
	st->lafs = lafs_alloc();
	if (st->verbose)
		printf("Filesystem state has been reset\n");
}


/****** NEWFS ******/
static char help_newfs[] = "Create a new LaFS filesystem, which can then be stored on one or more devices.";
static char *block_sizes[] = { "512", "1024", "2048", "4096", NULL };
static char *state_sizes[] = { "512", "1024", "2048", "4096", "8192",
			       "16384", "32768", NULL };
static struct args args_newfs[] = {
	{ "BLOCK-SIZE",  choice, -1, {.options=block_sizes},
	  "Block size, 512..4096, defaults to 1024"},
	{ "-block-size", choice,  0, {.options=block_sizes},
	  "Block size, 512..4096, defaults to 1024"},
	{ "-state-size", choice, -1, {.options=state_sizes},
	  "Size of state block, 512..32768, defaults to 1024"},
	{ "-uuid",       opaque, -1, {NULL}, "UUID - normally randomly generated"},
	{ "-black", opaque, -1, {NULL}, "nothing (just testing)"},
	TERMINAL_ARG
};
static void c_newfs(struct state *st, void **args)
{
	int blockbytes = 1024;
	int state_size = 0;
	char uuidstr[37];
	uuid_t uu;
	struct lafs_ino *ifile, *imfile, *rootdir;
	int create_atime = 1;

	if (st->lafs->blocksize) {
		printf("newfs: Filesytem already has state"
		       " - consider using \"reset\"\n");
		return;
	}

	if (args[1])
		/* As this is a 'choice' it must be a valid number. */
		get_int(args[1], &blockbytes);

	if (args[3]) {
		/* state-size was given */
		get_int(args[3], &state_size);
	}
	if (args[4]) {
		/* uuid was given */
		if (uuid_parse((char*)args[4], uu) < 0) {
			printf("newfs: UUID in wrong format: %s\n", (char*)args[4]);
			return;
		}
	}
	lafs_new(st->lafs, blockbytes);
	if (state_size)
		st->lafs->statesize = state_size;
	if (args[4])
		memcpy(st->lafs->uuid, uu, 16);

	ifile = lafs_get_itable(st->lafs);
	ifile->md.fs.usagetable = 1;
	lafs_dirty_inode(ifile);
	imfile = lafs_add_inode(ifile, 1, TypeInodeMap);
	rootdir = lafs_add_inode(ifile, 2, TypeDir);
	if (create_atime)
		lafs_add_inode(ifile, 3, TypeAccessTime);
	rootdir->md.file.linkcount = 2;
	rootdir->md.file.mode = 0755;
	rootdir->md.file.parent = 2;
	lafs_dirty_inode(rootdir);
	lafs_add_inode(ifile, 8, TypeOrphanList);

	lafs_imap_set(imfile, 1);
	lafs_imap_set(imfile, 2);
	lafs_imap_set(imfile, 8);

	lafs_cluster_init(st->lafs, 0, 0, 0, 1);

	if (st->verbose) {
		uuid_unparse(st->lafs->uuid, uuidstr);
		printf("Filesystem has been initilised: block size %d, "
		       "state size %d\n    UUID=%s\n",
		       st->lafs->blocksize, st->lafs->statesize,
		       uuidstr);
	}
	return;
}

/****** ADD_DEVICE ******/
static char help_add_device[] = "Add a device to the current LaFS";
static struct args args_add_device[] = {
/*1*/	{ "DEVNAME", external, -1, {NULL}, "Device to store filesystem on"},
/*2*/	{ "-file",   external, 0, {NULL}, "Regular file to use like a device"},
/*3*/	{ "-size",   opaque, -1, {NULL}, "Size of regular file (K,M,G prefix allowed)"},
/*4*/	{ "-segsize",opaque, -1, {NULL}, "Segment size for this device"},
/*5*/	{ "-stride", opaque, -1, {NULL}, "Stride (from one member device to next)"},
/*6*/	{ "-width",  opaque, -1, {NULL}, "Width of array in data-devices"},
/*7*/	{ "-usage_inum", opaque, -1, {NULL}, "Inode number for segment-usage file"},
/*8*/	{ "-force",  flag, -1, {NULL}, "Open device even if in-use"},
	TERMINAL_ARG
};
static void c_add_device(struct state *st, void **args)
{
	long block_bytes, segment_bytes = 0, stride_bytes = 0;
	int width = 0;
	long long device_bytes = 0;
	int usage_inum = 0;
	char *err = NULL;
	char *devname = args[1];
	int fd;
	struct lafs_device *dev;
	struct lafs_ino *ifile, *imfile, *segmap;

	if (!devname) {
		printf("add_device: No device or file name given to add\n");
		return;
	}

	block_bytes = st->lafs->blocksize;
	if (block_bytes == 0) {
		printf("add_device: filesystem is not ready for devices"
		       " - consider \"newfs\".\n");
		return;
	}

	ifile = st->lafs->ss.root;
	if (!ifile) {
		printf("add_device: filesystem has no root inode - strange"
		       " - consider \"newfs\" again.\n");
		return;
	}

	imfile = lafs_get_inode(ifile, 1);
	if (!imfile) {
		printf("add_device: Cannot find inode-map\n");
		return;
	}
	

	if (args[3]) {
		device_bytes = parse_size_print(args[3], &err, "add_device",
					   "file size");
		if (err)
			return;
	}
	if (args[4]) {
		segment_bytes = parse_size_print(args[4], &err, "add_device",
					   "segment size");
		if (err)
			return;
	}
	if (args[5]) {
		stride_bytes = parse_size_print(args[5], &err, "add_device",
					  "stride size");
		if (err)
			return;
	}
	if (args[6]) {
		width = parse_num_print(args[6], &err, "add_device", "width");
		if (err)
			return;
	}
	if (args[7]) {
		usage_inum = parse_num_print(args[7], &err,
					     "add_device", "inode number");
		if (err)
			return;
	}

	fd = open_device(devname, &device_bytes, args[2] != NULL,
			 args[8] != NULL, &err);

	if (fd < 0) {
		printf("add_device: %s\n", err);
		free(err);
		return;
	}

	err = lafs_validate_geometry(&block_bytes, &segment_bytes,
				     &stride_bytes, &width, device_bytes);

	if (err) {
		printf("add_device: %s\n", err);
		free(err);
		return;
	}

	if (!usage_inum) {
		usage_inum = lafs_imap_alloc(imfile);
	} else {
		if (lafs_imap_set(imfile, usage_inum) == 1) {
			printf("newfs: inum %d already in use.\n", usage_inum);
			return;
		}
	}
	dev = lafs_add_device(st->lafs, devname, fd,
			      segment_bytes / block_bytes,
			      stride_bytes / block_bytes,
			      width,
			      usage_inum);

	segmap = lafs_add_inode(ifile, usage_inum, TypeSegmentMap);
	if (segmap == NULL) {
		printf("ERROR: could not allocate segusage file.\n");
		st->lafs->flags |= LAFS_NEED_CHECK;
		return;
	}
	dev->segsum = segmap;
	segmap->md.segmentusage.table_size = dev->tablesize;
	lafs_dirty_inode(segmap);
	lafs_imap_set(imfile, usage_inum);

	if (st->verbose)
		printf("Added device %s at %llu with %llu segments "
		       "of %llu %dk blocks\n"
		       "    Usage inode %d\n",
		       devname, (unsigned long long)dev->start,
	       	       (unsigned long long)dev->segment_count,
		       (unsigned long long)dev->segment_size,
		       st->lafs->blocksize/1024, usage_inum);

}

/****** WRITE *****/
static struct cmd write_cmds[];

static char help_write[] = "Write content of various structures";
static struct args args_write[] = {
	{ "STRUCT", subcommand, -1, {write_cmds}, "Structure type to write"},
	TERMINAL_ARG
};
static void c_write(struct state *st, void **args)
{
	struct cmd *c = args[1];
	if (!c) {
		printf("write: Please give a structure to write\n");
		return;
	}

	c->cmd(st, args);
}

/****** WRITE_DEV ******/
static char help_write_dev[] = "Write devices blocks to one or all devices";
static struct args args_write_dev[] = {
	{ "DEVNAME", external, -1, {NULL}, "Device to write devblocks to"},
	{ "-all",    flag,      0, {NULL}, "Write to all devices"},
	TERMINAL_ARG
};
static void c_write_dev(struct state *st, void **args)
{
	struct lafs_device *dev;
	int found = 0;
	if (!args[2] && !args[3]) {
		printf("write dev: no device given for writing\n");
		return;
	}

	for (dev = st->lafs->devs; dev; dev = dev->next) {
		if (args[3] || strcmp(dev->name, (char*)args[2]) == 0) {
			int err = lafs_write_dev(dev);
			found = 1;
			if (err)
				printf("write dev: error when writing to %s\n",
				       dev->name);
			else if (st->verbose)
				printf("Device block written to %s\n", dev->name);
		}
	}
	if (found)
		return;
	if (args[3])
		printf("write dev: no devices exist to write to.\n");
	else
		printf("write dev: %s is not a registered device in this LaFS.\n",
		       (char*)args[2]);
}

/****** WRITE_STATE ******/
static char help_write_state[] = "Write state blocks to all devices";
static struct args args_write_state[] = {
	TERMINAL_ARG
};
static void c_write_state(struct state *st, void **args)
{
	if (st->lafs->blocksize == 0)
		printf("write state: filesystem is not initialised\n");
	else if (st->lafs->devs == NULL)
		printf("write state: No devices exist to write to\n");
	else if (lafs_write_state(st->lafs))
		printf("write state: Error writing a state block\n");
	else if (st->verbose)
		printf("%s state blocks written: seq now %llu\n",
		       (st->lafs->seq & 1) ? "Odd" : "Even",
		       (unsigned long long) st->lafs->seq);
}

/****** WRITE_CHECKPOINT ******/
static char help_write_checkpoint[] = "Write a checkpoint with all committed blocks";
static struct args args_write_checkpoint[] = {
	TERMINAL_ARG
};
static void c_write_checkpoint(struct state *st, void **args)
{
	if (st->lafs->blocksize == 0)
		printf("write checkpoint: filesystem is not initialised\n");
	else if (st->lafs->devs == NULL)
		printf("write checkpoint: No devices exist to write to\n");
	else if (st->lafs->free_head == st->lafs->free_tail)
		printf("write checkpoint: No free segments - try find_free\n");
	else if (lafs_checkpoint(st->lafs))
		printf("write state: Error writing checkpoint\n");
	else if (st->verbose)
		printf("Checkpoint written: seq now %llu\n",
		       (unsigned long long) st->lafs->seq);
}

#define WCMD(x) {#x, c_write_##x, args_write_##x, help_write_##x}
static struct cmd write_cmds[] = {
	WCMD(checkpoint),
	WCMD(dev),
	WCMD(state),
	{ NULL, NULL, NULL, NULL}
};

/****** LOAD_DEV ******/
static char help_load_dev[] = "Allow access to LaFS filesystem stored on given device";
static struct args args_load_dev[] = {
	{ "DEVNAME", external, -1, {NULL}, "Device to load filesystem info from"},
	{ "-file",   external,  0, {NULL}, "Regular file to load filesystem info from"},
	{ "-force",  flag,  -1, {NULL}, "Open device even if currently in use"},
	TERMINAL_ARG
};
static void c_load_dev(struct state *st, void **args)
{
	char *devname = args[1];
	int fd;
	long long device_bytes = 0;
	char *err;
	struct lafs_device *dev;

	if (!devname) {
		printf("load_dev: No device of file name given to load\n");
		return;
	}

	fd = open_device(devname, &device_bytes, args[2] != NULL,
			 args[3] != NULL, &err);

	if (fd < 0) {
		printf("load_dev: %s\n", err);
		free(err);
		return;
	}

	dev = lafs_load(fd, device_bytes, &err);

	if (err) {
		printf("load_dev: Cannot load %s: %s\n", devname, err);
		if (dev)
			talloc_free(dev);
		close(fd);
		return;
	}

	dev->name = talloc_strdup(dev, devname);

	if (lafs_include_dev(st->lafs, dev, &err) != 0) {
		printf("load_dev: Cannot include %s: %s\n", devname, err);
		talloc_free(dev);
		return;
	}
	if (st->verbose) {
		printf("loaded device %s - have %d of %d\n", devname,
		       st->lafs->loaded_devs, st->lafs->devices);
	}
}

/****** MOUNT ******/
static char help_mount[] = "Coalesce loaded devices into a filesystem";
static struct args args_mount[] = {
	{ "DEVNAME", external, -1, {NULL}, "Device to load and mount"},
	{ "-file", external, 0, {NULL}, "File to load and mount"},
	{ "-force", flag, -1, {NULL}, "Try to mount even if there are problems"},
	TERMINAL_ARG
};
static void c_mount(struct state *st, void **args)
{
	char *err;
	if (args[1]) {
		/* Load the device first, then mount */
		char *devname = args[1];
		long long device_bytes = 0;
		int fd;
		struct lafs_device *dev;
		if (st->lafs->blocksize) {
			printf("mount: lafs already initialised - cannot load %s\n",
			       devname);
			return;
		}
		fd = open_device(devname, &device_bytes, args[2] != NULL,
				 args[3] != NULL, &err);
		if (fd < 0) {
			printf("mount: %s\n", err);
			free(err);
			return;
		}
		dev = lafs_load(fd, device_bytes, &err);
		if (err) {
			printf("mount: Cannot load %s: %s\n", devname, err);
			if (dev)
				talloc_free(dev);
			close(fd);
			return;
		}
		dev->name = talloc_strdup(dev, devname);
		if (lafs_include_dev(st->lafs, dev, &err) != 0) {
			printf("mount: Cannot use %s: %s\n", devname, err);
			talloc_free(dev);
			return;
		}
		printf("loaded device %s\n", devname);
	}
	err = lafs_mount(st->lafs, args[3] != NULL);
	if (err) {
		printf("mount: cannot mount filesystem: %s\n", err);
		free(err);
	} else if (st->verbose)
		printf("filesystem mounted\n");
}

/****** SHOW *****/
static struct cmd show_cmds[];

static char help_show[] = "Show content of various structures";
static struct args args_show[] = {
	{ "STRUCT", subcommand, -1, {show_cmds}, "Structure type to show"},
	TERMINAL_ARG
};
static void c_show(struct state *st, void **args)
{
	struct cmd *c = args[1];
	if (!c) {
		printf("show: Please give a structure to show\n");
		return;
	}

	c->cmd(st, args);
}

/****** SHOW INODE ******/
static char help_show_inode[] = "Show inode given name or number";
static struct args args_show_inode[] = {
	{ "PATH", internal, -1, {NULL}, "Path to inode to show"},
	{ "-inum", opaque, 0, {NULL}, "Inode number to show"},
	TERMINAL_ARG
};
static void c_show_inode(struct state *st, void **args)
{
	struct lafs_ino *inode;
	char *err;
	if (!args[2]) {
		printf("show inode: please give file name or inode number\n");
		return;
	}
	inode = get_inode(st->lafs, args+2, &err);
	if (!inode) {
		printf("show inode: %s\n", err);
		free(err);
		return;
	}
	lafs_print_inode(inode);
}

/****** SHOW DEVICE ******/
static char help_show_device[] = "Show the device-block stored on a device";
static struct args args_show_device[] = {
	{ "DEVNAME", external, -1, {NULL}, "Device to show info for"},
	{ "-file", external, 0, {NULL}, "File to read as a device"},
	{ "-addr", opaque, -1, {NULL}, "Byte address to read block from"},
	TERMINAL_ARG
};
static void c_show_device(struct state *st, void **args)
{
	struct lafs_device *dev;
	char *devname = args[2];
	char *err;
	long long device_bytes = 0;
	long long addr;
	int fd;

	if (!devname && !st->lafs->devs) {
		printf("show device: no devices loaded - please give a device name\n");
		return;
	}
	if (args[4]) {
		char *addrstr = args[4];
		char *endp;
		struct lafs_dev dv;
		if (!devname) {
			printf("show device: device name must be given with address\n");
			return;
		}
		addr = strtoll(addrstr, &endp, 0);
		if (endp == addrstr || *endp) {
			printf("show device: %s is not a valid number\n",
			       addrstr);
			return;
		}
		fd = open_device(devname, &device_bytes, args[3] != NULL, 1, &err);
		if (fd < 0) {
			printf("show device: %s\n", err);	
			free(err);
			return;
		}
		if (lseek64(fd, addr, 0) != addr ||
		    read(fd, &dv, sizeof(dv)) != sizeof(dv)) {
			printf("show device: Cannot read device block at %lld on %s\n",
			       addr, devname);
		} else
			lafs_print_devblock(&dv);
		close(fd);
		return;
	}

	for (dev = st->lafs->devs; dev; dev = dev->next) {
		if (devname && strcmp(devname, dev->name) != 0)
			continue;
		printf("Device %s: %d out of %d\n", dev->name, 
		       dev->devnum, dev->devices);
		lafs_print_device(dev);
		if (devname)
			return;
	}
	if (!devname)
		return;
	/* Not already loaded, try to load-and-print */
	fd = open_device(devname, &device_bytes, args[3] != NULL, 1, &err);
	if (fd < 0) {
		printf("show device: %s\n", err);
		free(err);
		return;
	}
	dev = lafs_load(fd, device_bytes, &err);
	if (!dev) {
		printf("show device: Cannot load %s: %s\n",
		       devname, err);
		close(fd);
		return;
	}

	if (err)
		printf("Loaded device %s, however: %s\n", devname, err);
	else
		printf("Loaded device %s\n", devname);
	lafs_print_device(dev);
	talloc_free(dev);
}

/****** SHOW STATE ******/
static char help_show_state[] = "Show the current fs state or content of a state block";
static struct args args_show_state[] = {
	{ "DEVNAME", external, -1, {NULL}, "Device to find state block on"},
	{ "-file", external, 0, {NULL}, "File to read state block from"},
	{ "-addr", opaque, -1, {NULL}, "Byte address to read block from"},
	TERMINAL_ARG
};
static void c_show_state(struct state *st, void **args)
{
	char *devname;
	long long addr;
	struct lafs_device *dv;
	struct lafs_state *state;
	int fd;

	if (args[4] && !args[2]) {
		printf("show state: Cannot give address without device name\n");
		return;
	}
	if (!args[2]) {
		/* Show currently loaded state */
		if (!st->lafs->ss.root_addr) {
			printf("show state: LaFS not mounted, no state available\n");
			return;
		}
		lafs_print_lafs(st->lafs);
		return;
	}

	devname = args[2];
	if (args[4]) {
		char *addrstr = args[4];
		char *endp;
		long long device_bytes = 0;
		char *err;

		addr = strtoll(addrstr, &endp, 0);
		if (endp == addrstr || *endp) {
			printf("show state: %s is not a valid number\n",
			       addrstr);
			return;
		}
		dv = NULL;
		fd = open_device(devname, &device_bytes, args[3] != NULL, 1, &err);
		if (fd < 0) {
			printf("show state: %s\n", err);
			free(err);
			return;
		}
	} else {
		for (dv = st->lafs->devs; dv ; dv = dv->next)
			if (strcmp(devname, dv->name) == 0)
				break;
		if (!dv) {

			printf("show state: device %s not loaded and no address given\n",
			       devname);
			return;
		}
		fd = dv->fd;
		addr = dv->stateaddr[dv->recent_state];
	}
	state = malloc(st->lafs->statesize);
	if (lseek64(fd, addr, 0) != addr ||
	    read(fd, state, st->lafs->statesize) != st->lafs->statesize) {
		printf("show state: cannot load state block\n");
		return;
	}
	lafs_print_state(state, st->lafs->statesize);
	free(state);
	if (!dv)
		close(fd);
}

/****** SHOW CLUSTER ******/
static char help_show_cluster[] = "Show one or more cluster headers";
static struct args args_show_cluster[] = {
	{ "ADDR", opaque, -1, {NULL}, "Virtual address of cluster to display"},
	{ "-verbose", flag, -1, {NULL}, "Show fine detail of descriptors"},
	{ "-segment", flag, -1, {NULL}, "Show all following clusters in segment"},
	{ "-checkpoint", flag, -1, {NULL},  "Show following clusters in checkpoint"},
	{ "-reverse", flag, -1, {NULL}, "Follow backward chain rather than forward"},
	{ "-n", opaque, -1, {NULL}, "Show this many clusters"},
	TERMINAL_ARG
};
static void c_show_cluster(struct state *st, void **args)
{
	long long start, addr;
	int again = 1;
	int verbose = 0;
	int cnt;
	int max = 0;

	if (!st->lafs->blocksize) {
		printf("show cluster: lafs not ready to find clusters\n");
		return;
	}

	if (args[2] == NULL)
		start = st->lafs->checkpoint_cluster;
	else {
		char *addrstr = args[2];
		char *endp;
		start = strtoll(addrstr, &endp, 0);
		if (endp == addrstr || *endp) {
			printf("show cluster: %s is not a valid address\n", addrstr);
			return;
		}
	}
	if (args[7]) {
		char *nstr = args[7];
		char *endp;
		max = strtoll(nstr, &endp, 0);
		if (endp == nstr || *endp) {
			printf("show cluster: %s is not a valid number\n", nstr);
			return;
		}
	}

	addr = start;
	if (args[3])
		verbose = 1;
	while (again) {
		struct cluster_head *ch;
		long long prev;
		int err = lafs_load_cluster(st->lafs, addr, &ch);
		if (err < 0) {
			printf("show cluster: read error at %llu\n",
			       (unsigned long long) addr);
			return;
		}
		if (err == 1) {
			if (verbose)
				lafs_print_cluster(ch, st->lafs->blocksize, 0, 1);
			printf("(bad cluster header)\n");
			free(ch);
			return;
		}
		lafs_print_cluster(ch, st->lafs->blocksize, 1, verbose);
		cnt++;
		prev = addr;
		if (args[6])
			addr = __le64_to_cpu(ch->prev_addr);
		else
			addr = __le64_to_cpu(ch->next_addr);
		again = 0;
		if (args[4]) {
			int dev1, dev2;
			loff_t seg1, seg2, off1, off2;
			virttoseg(st->lafs, start, &dev1, &seg1, &off1);
			virttoseg(st->lafs, addr, &dev2, &seg2, &off2);
			if (dev1 == dev2 && seg1 == seg2)
				again = 1;
			/* Avoid going in wrong direction */
			if (args[6]) {
				if (addr >= prev)
					again = 0;
			} else {
				if (addr <= prev)
					again = 0;
			}
		}
		if (args[5]) {
			int f = __le32_to_cpu(ch->flags);
			if (f & CH_Checkpoint) {
				if (args[6] && !(f & CH_CheckpointStart))
					again = 1;
				if (!args[6] && !(f & CH_CheckpointEnd))
					again = 1;
			}
		}
		if (max && cnt < max)
			again = 1;
		if (addr == prev)
			again = 0;
		free(ch);
	}
}

/****** SHOW DIRBLOCK ******/
static char help_show_dirblock[] = "Show the detailed contents of directory block";
static struct args args_show_dirblock[] = {
	{ "BLOCK", opaque, -1, {NULL}, "Block number in file or LaFS"},
	{ "PATH", internal, -1, {NULL}, "Path of directory containing block"},
	{ "-inum", opaque, 1, {NULL}, "Inode number containing block"},
	TERMINAL_ARG
};
static void c_show_dirblock(struct state *st, void **args)
{
	int bnum;
	char *buf;
	char *bstr = args[2];

	if (!bstr) {
		printf("show dirblock: no block number given\n");
		return;
	}
	if (get_int(bstr, &bnum) < 0) {
		printf("show dirblock: %s is not a valid address\n", bstr);
		return;
	}
	if (!st->lafs->blocksize) {
		printf("show dirblock: LaFS not ready to show blocks\n");
		return;
	}
	if (args[3]) {
		char *err;
		struct lafs_dblk *db;
		struct lafs_ino *inode = get_inode(st->lafs, args+3, &err);
		if (!inode) {
			printf("show dirblock: %s\n", err);
			free(err);
			return;
		}
		db = lafs_dblk(inode, bnum);
		if (lafs_load_dblk(db)) {
			printf("show dirblock: cannot load block %d of inode %d\n",
			       bnum, inode->inum);
			return;
		}
		buf = db->b.data;
	} else {
		buf = malloc(st->lafs->blocksize);
		if (lafs_read_virtual(st->lafs, buf, bnum)) {
			printf("show dirblock: cannot read block at %d\n", bnum);
			free(buf);
			return;
		}
	}
	lafs_dir_print(buf, st->lafs->blockbits-8);
	if (!args[3])
		free(buf);
}

/****** SHOW SEGUSAGE ******/
static char help_show_segusage[] = "Show counts in segusage block";
static struct args args_show_segusage[] = {
	{ "BLOCK", opaque, -1, {NULL}, "Block number in file or LaFS"},
	TERMINAL_ARG
};
static void c_show_segusage(struct state *st, void **args)
{
	int bnum;
	char *buf;
	char *bstr = args[2];

	if (!bstr) {
		printf("show segusage: no block number given\n");
		return;
	}
	if (get_int(bstr, &bnum) < 0) {
		printf("show segusage: %s is not a valid address\n", bstr);
		return;
	}
	if (!st->lafs->blocksize) {
		printf("show segusage: LaFS not ready to show blocks\n");
		return;
	}

	buf = malloc(st->lafs->blocksize);
	if (lafs_read_virtual(st->lafs, buf, bnum)) {
		printf("show segusage: cannot read block at %d\n", bnum);
		free(buf);
		return;
	}

	lafs_print_segusage(buf, st->lafs->blocksize, 0, st->lafs->blocksize);
	if (!args[3])
		free(buf);
}

#define SCMD(x) {#x, c_show_##x, args_show_##x, help_show_##x}
static struct cmd show_cmds[] = {
	SCMD(cluster),
	SCMD(device),
	SCMD(dirblock),
	SCMD(inode),
	SCMD(segusage),
	SCMD(state),
	{ NULL, NULL, NULL, NULL}
};

/****** STORE ******/
static char help_store[] = "Create a file in the LaFS from an external file";
static struct args args_store[] = {
	{ "FROM", external, -1, {NULL}, "File to copy into LaFS"},
	{ "TO",   internal, -1, {NULL}, "Where to store file in LaFS"},
	{ "-from", external, 0, {NULL}, "File to copy into LaFS"},
	TERMINAL_ARG
};
static void c_store(struct state *st, void **args)
{
	char *from = args[1];
	char *to = args[2];
	struct lafs_ino *dir, *fs, *imfile, *inode;
	char *tail = NULL;
	int fd;
	loff_t bnum;
	u32 inum;

	if (!from) {
		printf("ERROR: Source file is missing\n");
		return;
	}
	if (!to) {
		printf("ERROR: destination file name is missing\n");
		return;
	}

	fs = lafs_get_itable(st->lafs);
	dir = lafs_get_inode(fs, 2);
	dir = lafs_lookup_path(dir, dir, to, &tail);
	if (!dir) {
		printf("store: lookup error in %s\n",to);
		return;
	}
	if (tail == NULL) {
		printf("store: %s already exists\n", to);
		return;
	}
	if (dir->type != TypeDir) {
		*tail = 0;
		printf("store: non-directory found at %s\n", to);
		return;
	}
	if (strchr(tail, '/') != NULL) {
		printf("store: non-final name does not exist: %s\n", tail);
		return;
	}

	fd = open(from, O_RDONLY);
	if (fd < 0) {
		printf("store: Cannot open %s: %s\n", from, strerror(errno));
		return;
	}

	imfile = lafs_get_inode(fs, 1);
	inum = lafs_imap_alloc(imfile);
	inode = lafs_add_inode(fs, inum, TypeFile);
	lafs_dir_add(dir, tail, inum, lafs_dt_type(inode));
	inode->md.file.linkcount = 1;
	lafs_dirty_inode(inode);

	bnum = 0;
	while(1) {
		struct lafs_dblk *db = lafs_dblk(inode, bnum);
		int n = read(fd, db->b.data, st->lafs->blocksize);
		if (n <= 0)
			break;
		inode->md.file.size = (bnum << st->lafs->blockbits) + n;
		db->b.flags |= B_Valid;
		lafs_dirty_blk(db);
		bnum++;
	}
	if (st->verbose)
		printf("Created %s as inode %d in %d\n", tail, inum, dir->inum);
}

/****** LS ******/
//static char *types[16] = { "unknown","FIFO","CHR","3","DIR","5","BLK","7","REG","9","LNK","11","SOCK","13","WHT","15"};
static char ctypes[] = "?pc3d5b7-9lBsDWF";
static char help_ls[] = "List files in a directory";
static struct args args_ls[] = {
	{"DIRECTORY", internal, -1, {NULL}, "Directory to list"},
	{"-inum", opaque, 0, {NULL}, "Inode number of directory to list"},
	{"-long", flag, -1, {NULL}, "Get a long, detailed listing"},
	TERMINAL_ARG
};
static void c_ls(struct state *st, void **args)
{
	struct lafs_ino *ino;
	u32 index = -1;
	char name[257];
	u32 inum;
	int type;
	int col;
	char *err;
	char *path = args[1];

	if (!path)
		args[1] = path = "";
	ino = get_inode(st->lafs, args+1, &err);
	if (!ino) {
		printf("ls: %s\n", err);
		free(err);
		return;
	}

	if (ino->type != TypeDir) {
		printf("ls: %s exists but is not a directory\n", path);
		return;
	}
	col = 0;
	while (lafs_dir_next(ino, &index, name, &inum, &type) == 1) {
		if (args[3]) {
			printf("%5lu %cxxxxxxxxx %s\n",
			       (unsigned long)inum, ctypes[type], name);
		} else {
			printf("%-12s ", name);
			col += 13;
			if (col > 60) {
				printf("\n");
				col = 0;
			}
		}
		if (index+1 == 0)
			break;
	}
	if (args[3] == NULL && col)
		printf("\n");
}

/****** FLUSH ******/
static char help_flush[] = "Flush out all changes to one or all files";
static struct args args_flush[] = {
	{ "PATH", internal, -1, {NULL}, "Path to inode to flush"},
	{ "-inum", opaque, 0, {NULL}, "Inode number to flush"},
	TERMINAL_ARG
};
static void c_flush(struct state *st, void **args)
{
	struct lafs_ino *inode;
	char *err;
	if (!args[1]) {
		lafs_flush(st->lafs);
		lafs_cluster_flush(st->lafs, 0);
		if (st->verbose)
			printf("Filesystem flushed\n");
		return;
	}
	inode = get_inode(st->lafs, args+1, &err);
	if (!inode) {
		printf("flush: %s\n", err);
		free(err);
		return;
	}
	lafs_flush_inode(inode);
	lafs_cluster_flush(st->lafs, 0);
	if (st->verbose)
		printf("Inode %d flushed\n", (int)inode->inum);
}

/****** TRACE ******/
static char help_trace[] = "Control internal tracing";
static struct args args_trace[] = {
	{ "LEVEL", opaque, -1, {NULL}, "New tracing verbosity level"},
	TERMINAL_ARG
};
static void c_trace(struct state *st, void **args)
{
	char *levelstr = args[1];
	int level = 1;
	extern int lafs_trace_level;
	int old = lafs_trace_level;
	if (levelstr) {
		if (get_int(levelstr, &level) < 0) {
			printf("trace: %s not a valid number\n", levelstr);
			return;
		}
	}
	if (st->verbose) {
		if (level == old)
			printf("Tracing unchanged at %d\n", old);
		else
			printf("Tracing change from %d to %d\n", old, level);
	}
	lafs_trace_level = level;
}

/****** SOURCE ******/
static char help_source[] = "Read commands from a file";
static struct args args_source[] = {
	{ "FILE", external, -1, {NULL}, "File to read commands from"},
	TERMINAL_ARG
};
static void c_source(struct state *st, void *args[])
{
	char *fname = args[1];
	FILE *f;
	if (!fname) {
		printf("source: no file name given\n");
		return;
	}
	f = fopen(fname, "r");
	if (f) {
		st->verbose = 0;
		runfile(st, f);
		st->verbose = 1;
		st->done = 0;
		fclose(f);
	} else
		printf("source: cannot open %s\n", fname);
}

/***********************************************************/
/* list of all commands - preferably in alphabetical order */
#define CMD(x) {#x, c_##x, args_##x, help_##x}
static struct cmd lafs_cmds[] = {
	{"?", c_help, args_help, help_help},
	CMD(add_device),
	CMD(exit),
	CMD(flush),
	CMD(help),
	CMD(load_dev),
	CMD(ls),
	CMD(mount),
	CMD(newfs),
	CMD(quit),
	CMD(reset),
	CMD(show),
	CMD(source),
	CMD(store),
	CMD(trace),
	CMD(write),
	{ NULL, NULL, NULL, NULL}
};

static struct args lafs_args[] = {
	{ "COMMAND", 	subcommand, -1, {lafs_cmds}, "Command for lafs to execute"},
	TERMINAL_ARG
};
