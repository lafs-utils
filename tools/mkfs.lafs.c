/*
 * mkfs.lafs - Create an empty LAFS filesystem
 *
 * Copyright (C) 2010 NeilBrown <neil@brown.name>
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    Author: Neil Brown
 *    Email: <neil@brown.name>
 *
 */

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <lafs/lafs.h>
#include <talloc.h>

#include "internal.h"

/*
 * A new filesystem must contain:
 *   inode 0 - TypeInodeFile with all the files listed here
 *   inode 1 - TypeInodeMap - empty
 *   inode 2 - TypeDir - empty
 *   inode 8 - TypeOrphanList - empty
 *   inode 16 - TypeSegmentMap - youth block and usage block for first segment
 *
 * These can all be in one checkpoint and could be in a single
 * write-cluster, though that could be a bit awkward.
 *
 * As well as the write-clusters we need device-blocks and state-blocks.
 *
 * Configurable values are:
 *  - block size, 512 to 4096  -b or --block-size
 *  - segment size - up to 64k, but multiple of width --segment-size
 *  - width - spindles across which device is striped --width
 *  - stride - chunk-size when device is striped --stride
 *
 */

enum {
	opt_segsize = 1000,
	opt_width,
	opt_stride,
	opt_noatime,
	opt_regular,
};

char short_options[] = "-b:Vvh";
struct option long_options[] = {
	{"block-size",   1, 0, 'b'},
	{"segment-size", 1, 0, opt_segsize},
	{"width",        1, 0, opt_width},
	{"stride",       1, 0, opt_stride},
	{"version",      0, 0, 'V'},
	{"verbose",      0, 0, 'v'},
	{"help",         0, 0, 'h'},
	{"no-atime-file",0, 0, opt_noatime},
	{"regular-file", 0, 0, opt_regular},
	{0, 0, 0, 0}
};

char version_text[] = "mkfs.lafs - unreleased\n";

char help_text[] =
	"mkfs.lafs: create a lafs filesystem\n"	
	;

char usage_text[] =
	"Usage: mkfs.lafs [options] device-name\n"
	" Options:\n"
	"   --block-size (-b)      size of basic block, up to 4096 (e.g. 2K)\n"
	"   --segment-size         size of segments, up to 65536 blocks (e.g. 32M)\n"
	"   --width                number of members of a striped device (e.g. 3)\n"
	"   --stride               chunk size of a striped device (e.g. 64K)\n"
	"   --help (-h)            This help message\n"
	"   --version (-V)         Report version information\n"
	"   --verbose (-v)         Be more verbose\n"
	;

static void get_size(long *valp, char *arg, char *name)
{
	long val;
	char *error;

	if (*valp != 0) {
		fprintf(stderr, "mkfs.lafs: %s has already been given,"
			" value \"%s\" not permitted.\n",
			name, arg);
		exit(2);
	}

	val = parse_size(arg, &error);
	if (error) {
		fprintf(stderr, "mkfs.lafs: %s: \"%s\" for %s\n",
			error, arg, name);
		exit(2);
	}
	*valp = val;
}

void get_num(int *valp, char *arg, char *name)
{
	long val;
	char *endp;

	if (*valp != 0) {
		fprintf(stderr, "mkfs.lafs: %s has already been given,"
			" value \"%s\" not permitted.\n",
			name, arg);
		exit(2);
	}

	val = strtol(arg, &endp, 0);
	if (endp == arg || *endp) {
		fprintf(stderr, "mkfs.lafs: Unrecognised number \"%s\""
			" for %s\n", arg, name);
		exit(2);
	}
	if (val == 0) {
		fprintf(stderr, "mkfs.lafs: 0 is not a valid number for %s\n", name);
		exit(2);
	}
	*valp = val;
}

int main(int argc, char *argv[])
{
	int verbose = 0;
	long block_bytes = 0;
	long segment_bytes = 0;
	int width = 0;
	long stride_bytes = 0;
	long long device_bytes;
	char *devname = NULL;
	int create_atime = 1;
	int regular_file = 0;
	int opt;
	int dev_fd;
	char *error;
	struct lafs *lafs;
	struct lafs_device *dev;
	struct lafs_ino *ifile, *imfile, *rootdir, *orphans, *segmap;
	struct lafs_ino *atimefile = NULL;

	while ((opt = getopt_long(argc, argv,
				  short_options, long_options,
				  NULL)) != -1) {
		switch(opt) {
		case 'h':
			fputs(help_text, stdout);
			fputs(usage_text, stdout);
			exit(0);
		case 'V':
			fputs(version_text, stdout);
			exit(0);
		case 'v':
			verbose++;
			break;
		case 'b':
			get_size(&block_bytes, optarg, "block size");
			break;
		case opt_segsize:
			get_size(&segment_bytes, optarg, "segment size");
			break;
		case opt_stride:
			get_size(&stride_bytes, optarg, "stride size");
			break;
		case opt_width:
			get_num(&width, optarg, "device width");
			break;
		case opt_noatime:
			create_atime = 0;
			break;
		case opt_regular:
			regular_file = 1;
			break;

		case 1:
			if (devname == NULL) {
				devname = optarg;
				break;
			}
			fprintf(stderr, "mkfs.lafs: multiple device names not supported: %s and %s\n",
				devname, optarg);
			exit(2);

		case '?':
		default:
			fputs(usage_text, stderr);
			exit(2);
		}
	}

	if (devname == NULL) {
		fputs("mkfs.lafs: no device name given\n", stderr);
		fputs(usage_text, stderr);
		exit(2);
	}

	/* Validate device */
	dev_fd = open_device(devname, &device_bytes, regular_file, 0, &error);
	if (dev_fd < 0) {
		fprintf(stderr, "mkfs.lafs: %s\n", error);
		free(error);
		exit(2);
	}

	/* Validate parameters */
	error = lafs_validate_geometry(&block_bytes, &segment_bytes,
				       &stride_bytes, &width,
				       device_bytes);
	if (error) {
		fprintf(stderr, "mkfs.lafs: %s\n", error);
		free(error);
		exit(2);
	}

	/* Create filesystem handle */
	lafs = lafs_alloc();

	/* Initialise filesystem */
	lafs_new(lafs, block_bytes);

	/* Add device */
	dev = lafs_add_device(lafs, devname, dev_fd,
			      segment_bytes / block_bytes,
			      stride_bytes / block_bytes,
			      width,
			      16);

	/* Write device blocks */
	lafs_write_dev(dev);

	/* create files */
	ifile = lafs_get_itable(lafs);
	ifile->md.fs.usagetable = 1;
	lafs_dirty_inode(ifile);
	imfile = lafs_add_inode(ifile, 1, TypeInodeMap);
	rootdir = lafs_add_inode(ifile, 2, TypeDir);
	if (create_atime)
		atimefile = lafs_add_inode(ifile, 3, TypeAccessTime);
	rootdir->md.file.linkcount = 2;
	rootdir->md.file.mode = 0755;
	rootdir->md.file.parent = 2;
	lafs_dirty_inode(rootdir);
	orphans = lafs_add_inode(ifile, 8, TypeOrphanList);
	segmap = lafs_add_inode(ifile, 16, TypeSegmentMap);
	lafs->devs->segsum = segmap;
	segmap->md.segmentusage.table_size = lafs->devs->tablesize;
	lafs_dirty_inode(segmap);

	lafs_imap_set(imfile, 1);
	lafs_imap_set(imfile, 2);
	lafs_imap_set(imfile, 8);
	lafs_imap_set(imfile, 16);

	lafs_cluster_init(lafs, 0, 0, 0, 1);
	/* Write checkpoint and state blocks */
	lafs_checkpoint(lafs);
	/* Write state blocks a second time, so all 4 copies are written */
	lafs_write_state(lafs);

	printf("Filesystem created with %llu segments of %llu %dK blocks\n",
	       (unsigned long long)dev->segment_count,
	       (unsigned long long)dev->segment_size,
	       lafs->blocksize/1024);

	talloc_free(lafs);

	exit(0);
}
