/*
 * fs/lafs/layout.h
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * layout of device superblock
 * and array state block
 */

/* All multibyte numerical values are in little-endian order
 */

/* The "superblock" describes a particular device in the filesystem.
 * different devices have different superblocks.
 */
struct lafs_dev {
	char	idtag[16];	/* LaFS-DeviceBlock */
	u8	uuid[16];
	u32	checksum;
	u32	seq;
	u32	version;	/* LAFS_DEV_VERS == 1 */
	u32	pad0;

	u64	ctime;
	u64	start, size;	/* in array address space (array block)*/
	u64	devaddr[2];	/* (device byte) one at each "end" */
	u64	stateaddr[4];	/* (device byte) 4 state blocks, two at each end */

	u8	statebits;	/* log of size of stateblock - normally 10-14 */
	u8	blockbits;	/* bits in fs block (byte)- 9 - 16 */
	u16	width;		/* devices in array - 1 to a few hundred */
	u32	stride;		/* Blocks in a stride - 1 to a very large number */
	u32	segment_size;	/* blocks in a segment (block) */
	u32	segment_offset;	/* offset of first segment (device byte) */
	u32	segment_count;
	u32	usage_inum;	/* inum of segment usage file */
	char	options[512];	/* Space separated, nul terminated, Cap for
				 * read-only-if-you-don't-understand
				 */
}  __attribute__((packed));
#define	LAFS_DEVBLK_SIZE	1024
#define	LAFS_DEV_VERS		1

struct lafs_state {
	char	idtag[16];	/* LaFS-State-Block */
	u8	uuid[16];
	u8	alt_uuid[16];	/* allows sharing of read-only devices
				 * between arrays
				 */
	u32	checksum;
	u32	version;	/* LAFS_STATE_VERS */
	u32	seq;
	u32	alt_seq;	/* seq number of the alt_uuid devices */
	u32	devices;
	u32	pad0;

	u32	nonlog_offset;	/* offset into above segment of next non-logged
				 * block to allocate
				 */
	u32	nonlog_segment;	/* segment number and */
	u16	nonlog_dev;	/* device number of active non-logged segment */
	u16	nextyouth;
	u32	maxsnapshot;

	u64	inconsistencies;/* bit set of discovered inconsistency types.
				 * If any unknown bits are set, an fsck is needed
				 * for writing.
				 * If known bits are set, behaviour is dependant
				 * on those bits.
				 */

	u64	checkpointcluster;	/* (array block) */
	char	options[512];	/* Spare separated, nul terminated. */
	u64	root_inodes[0];	/* (array block) */
} __attribute__((packed));
#define	LAFS_STATE_VERS	1

struct descriptor {
	u32	block_num;	/* (file block) */
	u16	block_cnt;	/* int */
	u16	block_bytes;	/* 0..blocksize - 0 means 'punch a hole',
				 * 1..blocksize means advance EOF to there
				 */
} __attribute__((packed));
#define	DescHole	0
#define	DescIndex	0xffff
#define	DescMiniOffset	0x8000

#define ROUND_UP(x)  (((x)+3)&~3)

struct miniblock {
	u32	block_num;	/* (file block) */
	u16	block_offset;	/* (byte) */
	u16	length;		/* (bytes) + 0x8000 */
	u8	data[0];
} __attribute__((packed));

struct group_head {
	u32	inum;
	u32	fsnum;
	u64	timestamp;	/* If non-zero, the ctime and mtime
				 * of the file should be set to this.
				 */
	u16	truncatenum_and_flag;
	u16	group_size_words;	/* 4byte words */
	union {
		struct descriptor desc[0];
		struct miniblock mb[0];
	} u;
}  __attribute__((packed));

struct cluster_head {
	char	idtag[8];	/* LaFSHead */
	u8	uuid[16];
	u64	seq;
	u32	flags;
	u16	verify_type;
	u16	Hlength;	/* header length - (bytes) */
	u32	Clength;	/* cluster length including header - (blocks) */
	u32	checksum;	/* over Hlength bytes */
	u8	verify_data[16];
	u64	next_addr;	/* (Array block) */
	u64	this_addr;	/* (array block) */
	u64	prev_addr;	/* (array block) */
	struct group_head groups[0];
}  __attribute__((packed));

#define	CH_Checkpoint		1
#define	CH_CheckpointStart	2
#define	CH_CheckpointEnd	4

/* values for verify_type */
#define	VerifyNull	0	/* if you found me, I'm valid */
#define	VerifyNext	1	/* if next head is valid, this cluster is */
#define	VerifyNext2	2	/* if next 2 heads are valid, this cluster is */
#define	VerifySum	3	/* maybe some sort of MIC is in _data */
#define	VerifyDevNext	4	/* If next head on this device is valud, this
				 * cluster is.
				 */
#define	VerifyDevNext2	5	/* if next 2 heads on this device are valid,
				 * this cluster is.
				 */

struct la_inode {
	/* 16 bytes is constant */
	u32	data_blocks;	/* (blocks) */
	u32	index_blocks;	/* (blocks) */
	u16	generation;
	u16	metadata_size;	/* (bytes) */
	u8	depth;
	u8	trunc_gen;
	u8	filetype;
	u8	flags;
#define	File_nonlogged	1
	union {
		struct fs_metadata {
			/* 52 bytes plus name */
			u64	update_time;
			u64	blocks_used; /* data+index */
			u64	blocks_allowed;
			u64	creation_age;
			u32	inodes_used;
			u32	parent;		/* Make it easier to look
						 * like a directory */
			u32	quota_inodes[3];
			u16	snapshot_usage_table;
			u16	pad;
			char	name[0];
		} fs;
		struct inodemap_metadata {
			u32	size;
		} inodemap;
		struct su_metadata {
			u32	table_size;	/* (blocks) */
		} segmentusage;
		struct file_metadata {
			u16	flags;
			u16	mode;
			u32	userid;
			u32	groupid;
			u32	treeid;
			u64	creationtime;
			u64	modifytime;
			u64	ctime;
			u64	accesstime;
			u64	size;
			u32	parent;
			u32	linkcount;
			u32	attrinode;
			u32	attributes[0];
		} __attribute__((packed)) file;
		struct dir_metadata {
			struct file_metadata h;
			u32	hash_seed;
			u32	attributes[0];
		} dir;
		struct special_metadata {
			struct file_metadata h;
			u32	major;
			u32	minor;
			u32	attributes[0];
		} special;
		struct quota_metadata {
			u32	gracetime; /* typically '7' */
			u32	graceunits; /* typically 24*60*60 */
		} quota;
	} metadata[0];
} __attribute__((packed));

#define LAFS_INODE_LOG_START offsetof(struct la_inode, metadata[0].file.flags)
#define LAFS_INODE_LOG_END offsetof(struct la_inode, metadata[0].file.size)
#define LAFS_INODE_LOG_SIZE (LAFS_INODE_LOG_END - LAFS_INODE_LOG_START)

#define	TypeInodeFile	1
#define	TypeInodeMap	2
#define	TypeSegmentMap	3
#define	TypeQuota	4
#define	TypeOrphanList	5
#define	TypeAccessTime	6

#define	TypeBase	16

#define	TypeFile	16
#define	TypeDir		17
#define	TypeSymlink	18
#define	TypeSpecial	19 /* char or block, or pipe or socket */
#define	TypeAttr	20

#define LAFS_MAX_LINKS ((1UL<<31)-1)

struct index {
	u32	logical;
	u32	phys_lo;
	u16	phys_hi;
} __attribute__((packed));

struct extent {
	u32	phys_lo;
	u16	phys_hi;
	u16	size;
	u32	logical;
}  __attribute__((packed));
#define	IBLK_INDEX (0)
#define IBLK_INDIRECT (1)
#define IBLK_EXTENT (2)

#define MaxDirHash 0x7fffffffUL
struct dirpiece {
	u32	target;	/* inode number */
	u8	next[2]; /* back, fore */
	u8	type:4, chain_info:2, longer:2;
/* 'longer' is 3 if fore and back the same length
 *   0  if back (next[0]) is longer
 *   1  if fore (next[1]) is longer
 * 'chain_info' is
 *   0,1: add that number to the hash of filename
 *   2  : add one trailing byte to hash
 *   3  : add 4 trailing bytes (little-endian) to hash
 */
#define Neither 3
	u8	length;
	char	name[0];
}  __attribute__((packed));

#define NoBlock (0xFFFFFFFF)
struct dirheader {
	u8	root;
	u8	lastpiece;
	u8	freepieces;
	u8	pad;
} __attribute__((packed));

/*
 * Miniblock for directory updates record the operation type
 * in the block_offset
 */
#define DIROP_LINK		0
#define	DIROP_UNLINK		1
#define	DIROP_REN_SOURCE	2
#define	DIROP_REN_NEW_TARGET	3
#define	DIROP_REN_OLD_TARGET	4

/*
 * The orphan file has a very simple structure with
 * 16 byte records identifying blocks in files that
 * might be orphans
 */
struct orphan {
	u32	type;	/* 0 if free */
	u32	filesys;
	u32	inum;
	u32	addr;
} __attribute__((packed));

/* Youth values are decayed when nextyouth gets too big */
static int inline decay_youth(int y)
{
	if (y < 8)
		return y;
	if (y < 32768+8)
		y = (y-8)/2 + 8;
	else
		y -= 16384;
	return y;
}
/* This is only called on large youth values */
static int inline decay_undo(int y)
{
	return y + 16384;
}

/* seg usage uses 4 bytes - so shift is 2
 * youth uses 2 bytes - so shift - 1
 */
#define	USAGE_SHIFT	2
#define	YOUTH_SHIFT	1
