#include <sys/types.h>
#include <lafs/list.h>
#include <stdint.h>

#define WC_NUM 2

#define HASH_BITS 8

struct lafs {
	uint8_t	uuid[16];
	uint32_t	seq;
	int	devices;
	int	blocksize;
	int	blockbits;
	int	statesize;
	int	max_segment;

	int	flags;
#define LAFS_DELAY_UPDATES	1
#define	LAFS_NEED_CHECK		2

	int checkpointing;
#define CHECKPOINTING 1
#define CHECKPOINT_START 2
#define CHECKPOINT_END 4
	loff_t	checkpoint_cluster;

	struct lafs_snapshot {
		struct lafs_snapshot *next;
		loff_t		root_addr;
		struct lafs_ino	*root;
		struct lafs_ino *rootdir;
	} ss;

	struct lafs_device *devs;
	int	loaded_devs;

	struct lafs_cluster {
		struct list_head blocks;
		loff_t		prev_addr;
		uint64_t	seq;
		int		remaining;

		char		*chead;
		int		chead_size;
		int		chead_blocks;

		struct lafs_segpos {
			int		dev;
			loff_t		num;

			uint32_t	st_table, st_row;
			uint32_t	nxt_table, nxt_row;
			uint32_t	table, row, col;

		} seg;
	} wc[WC_NUM];

	struct list_head leafs, account_leafs;
	struct list_head dirty_inodes;

	int	youth_next;

	struct list_head htable[1 << HASH_BITS];

	/* free segment list
	 * If free_head == free_tail, the list is empty
	 * Else free_head is the first to use and free_tail
	 * is the next to fill.
	 */
	int free_head, free_tail;
	struct {
		int dev;
		loff_t seg;
	} freelist[128];

	struct lafs_delayed {
		struct lafs_delayed *next;
		int dev;
		loff_t seg;
		int diff;
	} *delayed;
};


struct lafs_ino {
	struct lafs	*fs;
	struct lafs_dblk *dblock;
	struct lafs_iblk *iblock;
	int		inum;
	struct lafs_ino	*filesys;
	struct list_head dirty_inodes;
	struct list_head dirty;	/* blocks that might be dirty, or might belong
				 * to inodes with dirty blocks.
				 */

	u32	cblocks, /* data blocks which are commited to this file */
		ablocks, /* data blocks that are allocated */

		ciblocks; /* index blocks committed */

	int		iflags;
#define	I_Phase1 1	/* set to 'this' phase when iblocks correct */
#define	I_Dirty 2
/* next three indicate if we hold a reference on the relevant qent */
#define	I_QUid	8
#define	I_QGid	16
#define	I_QTid	32

	int		generation;
	int		trunc_gen;
	int		flags;

	int	type;
	int	metadata_size;
	int	depth;

	union {
		struct fs_md {
			int	usagetable;
			uint64_t	update_time;
			uint64_t	cblocks_used; /* blocks commited */
			uint64_t	ablocks_used; /* extra blocks allocated */
			uint64_t	blocks_allowed;
			uint64_t	blocks_unalloc;
			uint64_t	creation_age;
			uint32_t	parent;
			uint32_t	inodes_used;
			uint32_t	quota_inums[3];
			struct inode *quota_inodes[3];
			char		*name;
		} fs;
		struct inodemap_md {
			uint32_t	size;
		} inodemap;
		struct su_md {
			uint32_t	table_size;	/* (blocks) */
		} segmentusage;
		struct file_md {
			uint16_t	flags;
			uint16_t	mode;
			uint32_t	uid;
			uint32_t	gid;
			uint32_t	treeid;
			uint64_t	creationtime;
			uint64_t	modifytime;
			uint64_t	ctime;
			uint64_t	accesstime;
			uint64_t	i_accesstime; /* as stored in inode */
			uint64_t	size;
			uint32_t	parent;
			uint32_t	linkcount;
			/* for directories */
			uint32_t	seed;
			/* for special */
			uint32_t	major, minor;
			/* lists of blocks which need orphan-treatment */
			struct list_head dirorphan;
		} file;
		struct orphan_md {
			uint32_t	nextfree;
			uint32_t	reserved;
		} orphan;
		struct quota_md {
			uint32_t	gracetime; /* multiplier for graceunits */
			uint32_t	graceunits; /* seconds per unit */
		} quota;
	} md;
};

struct lafs_device {
	struct lafs_device *next;
	struct lafs *fs;
	int	fd;	
	int	seq;
	loff_t	start, size;
	loff_t	devaddr[2];
	loff_t	stateaddr[4];
	int	devnum;
	char	*name;

	struct timeval ctime;

	int	width;
	loff_t	stride, segment_size;
	loff_t	segment_offset, segment_stride, segment_count;
	int	usage_inum;

	int	rows_per_table, tables_per_seg;
	int	tablesize;

	int	recent_super; /* Index of most recent devaddr */
	int	recent_state; /* Index of most recent 'state' */
	int	state_seq;  /* 'seq' number of that state block */

	struct	lafs_ino *segsum;

	/* This are only used between loading from storage	
	 * and including in the array
	 */
	uint8_t	uuid[16];
	int	blockbits;
	int	statesize;
	int	devices;
};

struct lafs_blk {
	char		*data;
	int		flags;
	loff_t		fileaddr;
	loff_t		physaddr;

	struct lafs_ino *ino;
	struct list_head hash;

	struct lafs_iblk *parent;
	struct list_head siblings;
	struct list_head leafs;

	struct lafs_blk *chain;
};

struct lafs_dblk {
	struct lafs_blk b;
	struct lafs_ino *my_inode;
};

struct lafs_iblk {
	struct lafs_blk 	b;
	int			depth;
	struct list_head	children;
	struct lafs_blk		*uninc;
	int			sched_cnt;
};

#define B_Valid	1
#define	B_Dirty 2
#define B_Index 4
#define B_Sched 8
#define B_InoIdx 16
#define B_Uninc 32
