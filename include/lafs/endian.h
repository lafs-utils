#include <endian.h>
#define bswap_16(x) (((x) & 0x00ffU) << 8 | \
                     ((x) & 0xff00U) >> 8)
#define bswap_32(x) (((x) & 0x000000ffU) << 24 | \
                     ((x) & 0xff000000U) >> 24 | \
                     ((x) & 0x0000ff00U) << 8  | \
                     ((x) & 0x00ff0000U) >> 8)
#define bswap_64(x) (((x) & 0x00000000000000ffULL) << 56 | \
                     ((x) & 0xff00000000000000ULL) >> 56 | \
                     ((x) & 0x000000000000ff00ULL) << 40 | \
                     ((x) & 0x00ff000000000000ULL) >> 40 | \
                     ((x) & 0x0000000000ff0000ULL) << 24 | \
                     ((x) & 0x0000ff0000000000ULL) >> 24 | \
                     ((x) & 0x00000000ff000000ULL) << 8 | \
                     ((x) & 0x000000ff00000000ULL) >> 8)

#if BYTE_ORDER == LITTLE_ENDIAN
#define __cpu_to_le16(_x) (_x)
#define __cpu_to_le32(_x) (_x)
#define __cpu_to_le64(_x) (_x)
#define __le16_to_cpu(_x) (_x)
#define __le32_to_cpu(_x) (_x)
#define __le64_to_cpu(_x) (_x)

#define __cpu_to_be16(_x) bswap_16(_x)
#define __cpu_to_be32(_x) bswap_32(_x)
#define __cpu_to_be64(_x) bswap_64(_x)
#define __be16_to_cpu(_x) bswap_16(_x)
#define __be32_to_cpu(_x) bswap_32(_x)
#define __be64_to_cpu(_x) bswap_64(_x)
#elif BYTE_ORDER == BIG_ENDIAN
#define __cpu_to_le16(_x) bswap_16(_x)
#define __cpu_to_le32(_x) bswap_32(_x)
#define __cpu_to_le64(_x) bswap_64(_x)
#define __le16_to_cpu(_x) bswap_16(_x)
#define __le32_to_cpu(_x) bswap_32(_x)
#define __le64_to_cpu(_x) bswap_64(_x)

#define __cpu_to_be16(_x) (_x)
#define __cpu_to_be32(_x) (_x)
#define __cpu_to_be64(_x) (_x)
#define __be16_to_cpu(_x) (_x)
#define __be32_to_cpu(_x) (_x)
#define __be64_to_cpu(_x) (_x)
#else
#  error "unknown endianness."
#endif
