
#include <stdint.h>
#include <stdlib.h>

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t
#include <lafs/layout.h>
#include <lafs/struct.h>
#include <lafs/endian.h>

#define LAFS_NOBLOCK 0xFFFFFFFFUL

struct lafs *lafs_alloc(void);
int lafs_new(struct lafs *, int block_bytes);

char *lafs_validate_geometry(long *block_bytes,
			     long *segment_bytes,
			     long *stride_bytes,
			     int *width,
			     long long device_bytes);
struct lafs_device *lafs_add_device(struct lafs *, char *devname, int fd,
				    loff_t segblocks, loff_t strideblock,
				    int width, int usage_inum);
struct lafs_device *lafs_load(int fd, long long device_bytes, char **err);
int lafs_include_dev(struct lafs *fs, struct lafs_device *dev, char **err);

struct lafs_ino *lafs_get_itable(struct lafs *);
struct lafs_ino *lafs_add_inode(struct lafs_ino*, int inum, int type);
struct lafs_ino *lafs_get_inode(struct lafs_ino *fsys, int inum);
int lafs_imap_set(struct lafs_ino *, int inum);

int lafs_add_free_seg(struct lafs*, int dev, loff_t seg);
void lafs_find_free(struct lafs *fs);

void lafs_inode_init(struct lafs *, char *, int type);
struct lafs_ino *lafs_import_inode_buf(struct lafs *fs,
				       char *buf, int inum,
				       struct lafs_ino *parent);
void lafs_dirty_inode(struct lafs_ino *);
void lafs_inode_fillblock(struct lafs_ino *ino, char *buf);
void lafs_make_iblock(struct lafs_ino *ino);

struct lafs_dblk *lafs_dblk(struct lafs_ino *ino,
			   loff_t bnum);
int lafs_load_dblk(struct lafs_dblk *);
int lafs_find_dblk(struct lafs_dblk *);
struct lafs_ino *lafs_import_inode(struct lafs_dblk *db);
int lafs_load_cluster(struct lafs *fs, u64 addr, struct cluster_head **chp);

int lafs_read_virtual(struct lafs *, char *, loff_t);
int lafs_sched_blk(struct lafs_blk *);
int lafs_dirty_blk(struct lafs_dblk *);

int lafs_write_dev(struct lafs_device *dev);
int lafs_write_state(struct lafs *fs);
int lafs_checkpoint(struct lafs *fs);
void lafs_incorporate(struct lafs_iblk *ib);
void lafs_cluster_allocate(struct lafs_blk *b, int cnum);
int lafs_calc_cluster_csum(struct cluster_head *head);
int lafs_load_cluster(struct lafs *fs, u64 addr, struct cluster_head **chp);
void lafs_cluster_flush(struct lafs *fs, int cnum);
void lafs_flush_inode(struct lafs_ino *inode);
void lafs_flush(struct lafs *lafs);
int lafs_new_segment(struct lafs *, int cnum);
struct lafs_device *lafs_dev_find(struct lafs *fs, loff_t virt);
void lafs_allocated_block(struct lafs_blk *b, loff_t addr);
void lafs_cluster_init(struct lafs *fs, int cnum,
		       loff_t addr, loff_t prev, loff_t seq);
void lafs_summary_update(struct lafs_ino *ino,
			 loff_t oldaddr, loff_t newaddr,
			 int is_index);
void lafs_segment_count(struct lafs *fs, loff_t addr, int diff);
void lafs_segment_apply_delayed(struct lafs *fs);

void lafs_print_device(struct lafs_device *dev);
void lafs_print_devblock(struct lafs_dev *dev);
char *lafs_mount(struct lafs *fs, int force);
void lafs_print_state(struct lafs_state *state, int size);
void lafs_print_lafs(struct lafs *fs);
void lafs_print_inode(struct lafs_ino *ino);
void lafs_print_cluster(struct cluster_head *head, int blocksize,
			int groups, int verbose);
void lafs_print_segusage(char *buf, int blocksize, int start, int max);
u32 lafs_find_next(struct lafs_ino *ino, u32 bnum);
int lafs_hash_name(u32 seed, int len, const char *name);
void lafs_dir_init_block(char *block, int psz, const char *name, int len,
			 u32 target, int type, int chain_offset);
int lafs_dir_add_ent(char *block, int psz, const char *name, int len,
		     u32 target, int type, u32 seed, u32 hash, int hashoffset);
int lafs_dir_del_ent(char *block, int psz, u32 seed, u32 hash);
void lafs_dir_split(char *orig, int psz, char *new1, char *new2,
		    const char *name, u32 target, int type, u32 *newhash,
		    u32 seed, u32 hash, int chainoffset);
void lafs_dir_repack(char *block, int psz, char *new, u32 seed, int merge);
int lafs_dir_find(char *block, int psz, u32 seed, u32 hash, u8 *pp);
int lafs_dir_empty(char *block);
int lafs_dir_blk_size(char *block, int psz);
void lafs_dir_print(char *buf, int psz);

int lafs_dir_next(struct lafs_ino *dir, u32 *indexp, char *name,
		  u32 *inop, int *typep);
uint64_t lafs_leaf_lookup(unsigned char *buf, int len, u32 startaddr,
			  u32 target, u32 *nextp);
struct lafs_iblk *lafs_leaf_find(struct lafs_ino *inode,
				 u32 addr, int adopt, u32 *next);
u32 lafs_dir_lookup(struct lafs_ino *dir, char *name, int len);
struct lafs_ino *lafs_lookup_path(struct lafs_ino *root, struct lafs_ino *cwd,
				  char *path, char **remainder);
int lafs_imap_alloc(struct lafs_ino *imap);
int lafs_dir_add(struct lafs_ino *dir, char *name, u32 inum, int type);




static inline struct lafs_dblk *dblk(struct lafs_blk *b)
{
	return container_of(b, struct lafs_dblk, b);
}

static inline struct lafs_iblk *iblk(struct lafs_blk *b)
{
	return container_of(b, struct lafs_iblk, b);
}

static inline uint64_t lafs_encode_timeval(struct timeval *tm)
{
	uint64_t nano = tm->tv_usec * 1000;
	uint64_t sec = tm->tv_sec & (0x7FFFFFFFFULL);
	nano &= ~1ULL;
	return sec | (nano << 34);
}
static inline void lafs_decode_timeval(struct timeval *tm, uint64_t te)
{
	/* low 35 bits are seconds (800 years)
	 * high 29 bits are 2nanoseconds
	 */
	long nano;
	tm->tv_sec = (te & 0X7FFFFFFFFULL);
	nano = (te >> 34) & ~(long)1;
	tm->tv_usec = nano / 1000;
}

static inline struct lafs_device *dev_by_num(struct lafs *fs, int num)
{
	struct lafs_device *dv;
	for (dv = fs->devs ; dv ; dv = dv->next)
		if (dv->devnum == num)
			return dv;
	return NULL;
}


static inline void
virttoseg(struct lafs *fs, loff_t virt, int *devp, loff_t *segp, loff_t *offsetp)
{
	struct lafs_device *dv = lafs_dev_find(fs, virt);

	virt -= dv->start;
	if (dv->segment_size >= dv->width * dv->stride) {
		*offsetp = virt % dv->segment_size;
		*segp = virt / dv->segment_size;
	} else {
		int of = virt % dv->stride;
		int strp =virt / (dv->width * dv->stride);

		*segp = (strp * dv->stride + of) /
			(dv->segment_size / dv->width);
		*offsetp = virt - dv->segment_stride * *segp;
	}
	*devp = dv->devnum;
}

static inline void
virttophys(struct lafs *fs, loff_t virt, int *devp, loff_t *sectp)
{
	struct lafs_device *dv = lafs_dev_find(fs, virt);

	if (dv == NULL)
		return;
	*devp = dv->devnum;

	virt -= dv->start;
	virt *= fs->blocksize;
	virt += dv->segment_offset;
	*sectp = virt;
}

#include <dirent.h>

static inline int lafs_dt_type(struct lafs_ino *ino)
{
	if (ino->type < TypeBase)
		return 0;
	switch(ino->type) {
	default: return 0;
	case TypeFile: return DT_REG;
	case TypeDir: return DT_DIR;
	case TypeSymlink: return DT_LNK;
	case TypeSpecial: return ino->md.file.mode >> 12;
	}
}
