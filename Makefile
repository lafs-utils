
all:
	$(MAKE) -C lib/
	$(MAKE) -C tools/

clean:
	$(MAKE) -C lib/ clean
	$(MAKE) -C tools/ clean
