
/*
 * Connect this dblk into indexing tree and find
 * its phys addr.
 * After this call, ->parent must be set, and ->physaddr must
 * be correct
 *
 * FIXME
 * For now we only work with indexing directly from the inode
 * Separate index blocks are not yet supported
 */

#include <stdlib.h>
#include <lafs/lafs.h>
#include <talloc.h>
#include "internal.h"

u64 lafs_leaf_lookup(unsigned char *buf, int len, u32 startaddr,
			    u32 target, u32 *nextp);

int lafs_find_dblk(struct lafs_dblk *db)
{
	struct lafs_ino *ino = db->b.ino;
	struct lafs *fs = ino->fs;

	if (db->b.parent)
		return 0;

	if (!ino->iblock)
		lafs_make_iblock(ino);

	if (ino->depth > 1)
		abort();

	if (ino->filesys != ino ||
	    db->b.fileaddr != 0) {
		db->b.parent = ino->iblock;
		(void)talloc_reference(db, ino->iblock);
		list_add(&db->b.siblings, &ino->iblock->children);
	} else
		db->b.parent = NULL;

	if (ino->depth == 0) {
		db->b.physaddr = 0;
		return 0;
	}

	db->b.physaddr = lafs_leaf_lookup(
		(unsigned char*)ino->dblock->b.data + ino->metadata_size,
		fs->blocksize - ino->metadata_size,
		0,
		db->b.fileaddr,
		NULL);
	return 0;
}


uint64_t lafs_leaf_lookup(unsigned char *buf, int len, u32 startaddr,
			  u32 target, u32 *nextp)
{
	/* buf starts with a 2byte header
	 * if 1, then 4 byte offset followed by  6byte littleending indirect entries.
	 * if 2, then 12byte extent entries
	 */
	uint64_t p;
	unsigned char *cp;
	int hi,lo;
	int elen;
	u32 addr, next;

	if (nextp)
		*nextp = NoBlock;
	if (buf[1])
		return 0;
	switch (buf[0]) {
	default: p = 0;
		break;

	case 1: /* indirect */
		len -= 2+4;
		buf += 2;

		if ( target < startaddr)
			return 0;

		next = target;
		target -= startaddr;

		if (target*6 + 6 > len)
			return 0;
		buf += target*6;
		p = decode48(buf);
		if (nextp) {
			/* find the next allocated block */
			uint64_t p2;

			len -= target*6;
			len -= 6;
			next++;
			*nextp = NoBlock;
			while (len > 6) {
				p2 = decode48(buf);
				if (p2) {
					*nextp = next;
					break;
				}
				len -= 6;
				next++;
			}
		}
		break;

	case 2: /* extent */
		/* 12 byte records: 6byte device, 2 byte length, 4 byte fileaddr */

		len -= 2;
		buf += 2;
		lo = 0;
		hi = len/12;
		while (hi > lo+1) {
			int mid = (lo+hi)/2;
			int len;
			cp = buf;
			cp += mid*12 + 6;

			len = decode16(cp);
			addr = decode32(cp);

			if (len && addr <= target)
				lo = mid;
			else
				hi = mid;
		}
		cp = buf;
		cp += lo*12 + 8;

		addr = decode32(cp);

		cp = buf;
		cp += lo*12+6;
		elen = decode16(cp);
		if (target < addr || target >= addr + elen)
			p = 0;
		else {
			cp = buf;
			cp += lo*12;
			p = decode48(cp);
			p += target - addr;
		}

		if (nextp) {
			if (lo == len/12)
				*nextp = NoBlock; /* next extent */
			else {
				uint64_t p2;
				cp = buf;
				cp += (lo+1)*12;
				p2 = decode48(cp);
				len = decode16(cp);
				if (len == 0)
					*nextp = NoBlock; /* no more meaningful extents*/
				else
					*nextp = decode32(cp);
			}
		}
	}
	return p;
}

