#include <lafs/lafs.h>

struct lafs_ino *lafs_import_inode(struct lafs_dblk *db)
{
	struct lafs_ino *parent = db->b.ino;
	struct lafs_ino *ino;

	ino = lafs_import_inode_buf(parent->fs,
				    db->b.data, db->b.fileaddr,
				    parent);

	if (ino)
		ino->dblock = db;
	db->my_inode = ino;
	return ino;
}
