
/*
 * Updates summary info for this newly written block.
 * This includes:
 *   block count in the inode
 *   block count in the fs
 *   quotas
 *   segment usage.
 */

#include <lafs/lafs.h>

void lafs_summary_update(struct lafs_ino *ino,
			 loff_t oldaddr, loff_t newaddr,
			 int is_index)
{
	struct lafs *fs = ino->fs;
	int diff;

	if (oldaddr && newaddr) {
		/* Not a new allocation */
		lafs_segment_count(fs, oldaddr, -1);
		lafs_segment_count(fs, newaddr, 1);
		return;
	}
	if (!oldaddr && !newaddr)
		/* nothing changing! */
		return;

	if (oldaddr)
		diff = -1;
	else
		diff = 1;

	if (is_index)
		ino->ciblocks += diff;
	else
		ino->cblocks += diff;

	if (!is_index)
		if (diff > 0)
			ino->ablocks--;

	ino = ino->filesys;
	ino->md.fs.cblocks_used++;
	if (!is_index)
		if (diff > 0)
			ino->md.fs.ablocks_used--;

	if (oldaddr)
		lafs_segment_count(fs, oldaddr, -1);
	if (newaddr)
		lafs_segment_count(fs, newaddr, 1);
}

