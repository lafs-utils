
/*
 * Internal use defines for liblafs - not externally visible
 */
#include <stdio.h>
#include <strings.h>

extern int lafs_trace_level;

#define trace(n, str ...) \
	do { if (lafs_trace_level >= n) fprintf(stderr, str); } while(0)


static inline void
de_sched(struct lafs_blk *b)
{
	if (!(b->flags & B_Sched))
		return;
	b->flags &= ~B_Sched;

	if (b->parent) {
		struct lafs_iblk *p = b->parent;
		trace(2,"desched %d/%lld %d/%lld %d\n", b->ino->inum, (long long)b->fileaddr, p->b.ino->inum, (long long)p->b.fileaddr,
		      p->sched_cnt);
		p->sched_cnt--;
		if (p->sched_cnt == 0)
			lafs_sched_blk(&p->b);
	}
}

unsigned long crc32(
	unsigned long crc,
	const uint32_t *buf,
	unsigned len);


/*
 * extract little-endian values out of memory.
 * Each function is given a char*, and moves it forwards
 */

#define decode16(p) ({ unsigned int _a; _a= (unsigned char)*(p++); _a + (((unsigned char)*p++)<<8); })
#define decode32(p) ({ long _b; _b = decode16(p); _b + ((u32)decode16(p)<<16); })
#define decode48(p) ({ u64 _c; _c = decode32(p); _c + ((u64)decode16(p)<<32); })


#define encode16(p,n) ({ *(p++) = (n)&255; *(p++) = ((n)>>8) & 255; })
#define encode32(p,n) ({ encode16(p,n); encode16(p, ((n)>>16)); })
#define encode48(p,n) ({ encode32(p,n); encode16(p, ((n)>>32)); })

struct dir_ent {
	char *name;
	int nlen;
	u32 target;
	int type;
};
struct dir_ent *
lafs_dir_extract(char *block, int psz, struct dir_ent *de, int pnum,
		 u32 *hash);
void lafs_dir_set_target(char *block, int psz, struct dir_ent *de, int pnum);
void lafs_dir_print(char *buf, int psz);
#define DT_TEST 128 /* for dir_add_ent, this flags to test for space, but not do
                     * any actual add
                     */
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

#define le32_to_cpu __le32_to_cpu
#define cpu_to_le32 __cpu_to_le32
#define printk printf

static inline void BUG_ON(int cond)
{
	if (cond)
		abort();
}
static inline void BUG(void)
{
	BUG_ON(1);
}
static inline void set_bit(int bit, unsigned long *map)
{
	*map |= 1UL<<bit;
}

static inline void clear_bit(int bit, unsigned long *map)
{
	*map &= ~(1UL<<bit);
}

static inline int test_bit(int bit, unsigned long *map)
{
	return *map & (1UL<<bit);
}


static inline int set_c_bit(int nr, unsigned char * addr)
{
	int	mask, retval;

	addr += nr >> 3;
	mask = 1 << (nr & 0x7);

	retval = (mask & *addr) != 0;
	*addr |= mask;

	return retval;
}

static inline int clear_c_bit(int nr, unsigned char * addr)
{
	int	mask, retval;

	addr += nr >> 3;
	mask = 1 << (nr & 0x7);

	retval = (mask & *addr) != 0;
	*addr &= ~mask;

	return retval;
}

static inline int test_c_bit(int nr, const unsigned char * addr)
{
	int	mask;

	addr += nr >> 3;
	mask = 1 << (nr & 0x7);
	return ((mask & *addr) != 0);
}

static inline int find_first_bit(char *buf, int size)
{
	int b = 0;
	while (size && *buf == 0) {
		size--;
		buf++;
		b += 8;
	}
	if (!size)
		return b;
	return b + ffs(*buf)-1;
}
