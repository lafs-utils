
#include <lafs/lafs.h>

static int __block_find_next(struct lafs_ino *inode, u32 *addrp);

u32 lafs_find_next(struct lafs_ino *inode, u32 addr)
{
	/* Find the first allocated block in 'ino'
	 * which is at-or-after 'addr'.
	 * making sure to skip over Recent hole.
	 */
	while(1) {
		switch(__block_find_next(inode, &addr)) {
		case 0:
			return LAFS_NOBLOCK;
		case 1:
			return addr;
		case 2:
			continue;
		}
	}
}

static int __block_find_next(struct lafs_ino *inode, u32 *addrp)
{
	/* find the next allocated block in inode at or after '*addrp'
	 * and set *addrp.
	 * Return:
	 *      0 - there is no block at or after *addrp
	 *	1 - Yes, *addrp exists
	 *      2 - *addrp has been updated, check again
	 */
	struct lafs_iblk *leaf;
	u32 nexti, nextd;
	u64 p;
	int offset;
	struct lafs_blk *b;
	u32 addr = *addrp;
	int rv = 0;

	if (inode->depth == 0) {
		if (addr == 0)
			return 1;
		lafs_make_iblock(inode);
		leaf = inode->iblock;
	} else {
		unsigned char *buf;
		leaf = lafs_leaf_find(inode, addr, 0, &nexti);
		if (nexti != LAFS_NOBLOCK)
			rv = 2;
		offset = 0;
		buf = (unsigned char *)leaf->b.data;
		if (inode->depth == 1) {
			offset = inode->metadata_size;
			buf = (unsigned char *)leaf->b.ino->dblock->b.data;
		}
		p = lafs_leaf_lookup(buf + offset,
				     inode->fs->blocksize - offset, leaf->b.fileaddr,
				     addr, &nextd);
		if (p)
			return 1;

		if (nextd == LAFS_NOBLOCK)
			nextd = nexti;
		else
			rv = 1;
	}
	list_for_each_entry(b, &leaf->children, siblings) {
		if (b->fileaddr >= addr
		    && b->fileaddr < nextd
		    && (b->flags & B_Valid)) {
			nextd = b->fileaddr;
			rv = 1;
		}
	}
	*addrp = nextd;
	return rv;
}
