
/*
 * schedule a block for writeout in the next checkpoint
 */

#include <lafs/lafs.h>

#include <stdio.h>
#include "internal.h"


int lafs_sched_blk(struct lafs_blk *blk)
{
	struct lafs *fs = blk->ino->fs;

	blk->flags |= B_Dirty;
	if (blk->flags & B_Sched) {
		if (list_empty(&blk->leafs))
			list_add_tail(&blk->leafs, &fs->leafs);
		return 0;
	}

	if (!(blk->flags & B_Index)) {
		struct lafs_dblk *dblk = container_of(blk, struct lafs_dblk, b);
		if (lafs_find_dblk(dblk) < 0)
			return -1;
	}
	blk->flags |= B_Sched;
	if (blk->parent) {
		blk->parent->sched_cnt++;
		trace(1, "schedule %d/%d %d/%d %d\n", blk->ino->inum, (int)blk->fileaddr,
		      blk->parent->b.ino->inum, (int)blk->parent->b.fileaddr,
		      blk->parent->sched_cnt);
	} else
		trace(1, "schedule %d/%d\n", blk->ino->inum, (int)blk->fileaddr);
	list_move_tail(&blk->leafs, &fs->leafs);
	return 0;
}
