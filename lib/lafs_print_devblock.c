#include <uuid/uuid.h>
#include <lafs/lafs.h>
#include <stdio.h>
#include "internal.h"

void lafs_print_devblock(struct lafs_dev *dev)
{
	char uuidstr[37];
	time_t atime;
	u32 crc, crc2;
	printf("Superblock:\n");
	printf(" IdTag      : %.16s\n", dev->idtag);
	printf(" Version    : %d\n", (int)__le32_to_cpu(dev->version));
	uuid_unparse(dev->uuid, uuidstr);
	printf(" UUID       : %s\n", uuidstr);
	printf(" Checksum   : %08x", (unsigned int)__le32_to_cpu(dev->checksum));
	crc = dev->checksum;
	dev->checksum = 0;
	crc2 = crc32(0, (uint32_t*)dev, LAFS_DEVBLK_SIZE);
	dev->checksum = crc;
	if (crc2 == crc)
		printf(" (correct)\n");
	else
		printf(" (expected %08x)\n", (unsigned int)__le32_to_cpu(crc2));
	printf(" Seq        : %ld\n", (unsigned long)__le32_to_cpu(dev->seq));
	atime = (time_t)__le64_to_cpu(dev->ctime);
	printf(" Creation   : %.24s\n", ctime(&atime));
	printf(" Start	    : %llu\n", (unsigned long long)__le64_to_cpu(dev->start));
	printf(" Size       : %llu\n", (unsigned long long)__le64_to_cpu(dev->size));
	printf(" Stride     : %lu\n", (unsigned long)__le32_to_cpu(dev->stride));
	printf(" Width      : %lu\n", (unsigned long) __le32_to_cpu(dev->width));
	printf(" StateBits  : %d\n", dev->statebits);
	printf(" Devblk[0]  : %llu\n", (unsigned long long)__le64_to_cpu(dev->devaddr[0]));
	printf(" Devblk[1]  : %llu\n", (unsigned long long)__le64_to_cpu(dev->devaddr[1]));
	printf(" State[0]   : %llu\n", (unsigned long long)__le64_to_cpu(dev->stateaddr[0]));
	printf(" State[1]   : %llu\n", (unsigned long long)__le64_to_cpu(dev->stateaddr[1]));
	printf(" State[2]   : %llu\n", (unsigned long long)__le64_to_cpu(dev->stateaddr[2]));
	printf(" State[3]   : %llu\n", (unsigned long long)__le64_to_cpu(dev->stateaddr[3]));
	printf(" SegSize    : %lu\n", (unsigned long)__le32_to_cpu(dev->segment_size));
	printf(" SegOffs    : %lu\n", (unsigned long)__le32_to_cpu(dev->segment_offset));
	printf(" SegCount   : %lu\n", (unsigned long)__le32_to_cpu(dev->segment_count));
	printf(" BlkBits    : %lu\n", (unsigned long)__le32_to_cpu(dev->blockbits));
	printf(" UsageInum  : %lu\n", (unsigned long)__le32_to_cpu(dev->usage_inum));
	printf("\n");
}

