
/*
 * Mark an inode number as being in-use in the inode map.
 * If the number is more than one block beyond end-of-file,
 * we fail.
 */

#include <lafs/lafs.h>
#include <memory.h>
#include <stdio.h>
#include "internal.h"

int lafs_imap_set(struct lafs_ino *ino, int inum)
{
	struct lafs *fs = ino->fs;
	struct lafs_dblk *db;
	int blknum = (inum / fs->blocksize) / 8;

	if (ino->type != TypeInodeMap)
		return -1;
	if (blknum > ino->md.inodemap.size)
		return -1;

	trace(2, "imap set %d %d %d\n", inum, blknum, ino->md.inodemap.size);
	db = lafs_dblk(ino, blknum);
	lafs_load_dblk(db);
	if (blknum == ino->md.inodemap.size) {
		memset(db->b.data, 0xff, fs->blocksize);
		ino->md.inodemap.size++;
		lafs_dirty_inode(ino);
	}
	inum -= blknum * fs->blocksize * 8;
	if (!test_c_bit(inum, (unsigned char*)db->b.data))
		return 1;
	clear_c_bit(inum, (unsigned char*)db->b.data);
	/* FIXME if block is now empty, punch a hole */
	lafs_sched_blk(&db->b);
	return 0;
}
