
/*
 * Create an inode in the file with the given inum and type.
 * We get the data block, file it in, and import it.
 */

#include <lafs/lafs.h>


struct lafs_ino *lafs_add_inode(struct lafs_ino *fsys, int inum, int type)
{
	struct lafs_dblk *db;
	struct lafs_ino *ino;

	db = lafs_dblk(fsys, inum);
	if (db->my_inode)
		return NULL;

	lafs_inode_init(fsys->fs, db->b.data, type);
	db->b.flags |= B_Valid;

	ino = lafs_import_inode(db);
	lafs_sched_blk(&db->b);
	return ino;
}
