#include <string.h>
#include <lafs/lafs.h>
#include "internal.h"

int lafs_dir_next(struct lafs_ino *dir, u32 *indexp, char *name,
		  u32 *inop, int *typep)
{
	/* Find the first entry after *indexp, and set indexp,
	 * name, inop, typep to that entry.
	 * Return 0 on success or 1 if no more entries.
	 * If called with *indexp == -1, look for first entry.
	 */
	u32 hash = *indexp + 1;

	do {
		struct lafs_dblk *db;
		u32 block;
		u8 piece;
		struct dir_ent de;
		block = lafs_find_next(dir, hash+1);
		if (block == LAFS_NOBLOCK)
			block = 0;

		db = lafs_dblk(dir, block);
		if (!db ||
		    lafs_load_dblk(db))
			return -1;

		while (1) {
			lafs_dir_find(db->b.data, dir->fs->blockbits-8,
				      dir->md.file.seed,
				      hash, &piece);
			*indexp = dir->md.file.seed;
			if (piece &&
			    lafs_dir_extract(db->b.data, dir->fs->blockbits-8, &de,
					     piece, indexp)->target == 0)
				hash = *indexp +1;
			else
				break;
		}
		if (piece) {
			if (de.target) {
				strncpy(name, de.name, de.nlen);
				name[de.nlen] = 0;
				*inop = de.target;
				*typep = de.type;
				return 1;
			}
		}
		hash = block;
	} while (hash);
	return 0;
}
