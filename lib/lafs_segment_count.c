
/* lafs_segment_count
 * adjust the usage count for the segment containing a given block
 */

#include <lafs/lafs.h>

static void delay_update(struct lafs *fs, int dev, loff_t seg, int diff)
{
	struct lafs_delayed *d = fs->delayed;

	while (d && (d->dev != dev || d->seg != seg))
		d = d->next;
	if (!d) {
		d = malloc(sizeof(*d));
		d->next = fs->delayed;
		fs->delayed = d;
		d->dev = dev;
		d->seg = seg;
		d->diff = 0;
	}
	d->diff += diff;
}

void segment_count(struct lafs *fs, int dev, loff_t seg, int diff)
{
	struct lafs_device *dv;
	struct lafs_dblk *db;
	uint32_t *p;
	int cnt;
	loff_t addr;

	dv = dev_by_num(fs, dev);
	addr = dv->tablesize + seg / (fs->blocksize >> USAGE_SHIFT);

	db = lafs_dblk(dv->segsum, addr);
	lafs_load_dblk(db);
	p = (void*)db->b.data;
	cnt = __le32_to_cpu(p[seg % (fs->blocksize >> USAGE_SHIFT)]);
	cnt += diff;
	p[seg % (fs->blocksize >> USAGE_SHIFT)] = __cpu_to_le32(cnt);
	lafs_sched_blk(&db->b);
}


void lafs_segment_count(struct lafs *fs, loff_t addr, int diff)
{
	int dev;
	loff_t seg;
	loff_t offset;

	virttoseg(fs, addr, &dev, &seg, &offset);

	if (fs->flags & LAFS_DELAY_UPDATES) {
		delay_update(fs, dev, seg, diff);
		return;
	}
	segment_count(fs, dev, seg, diff);
}

void lafs_segment_apply_delayed(struct lafs *fs)
{
	struct lafs_delayed *delayed = fs->delayed;

	if (fs->flags & LAFS_DELAY_UPDATES)
		abort();
	fs->delayed = NULL;

	while (delayed) {
		struct lafs_delayed *d = delayed;
		delayed = d->next;
		segment_count(fs, d->dev, d->seg, d->diff);
		free(d);
	}
}
