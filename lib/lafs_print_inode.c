#include <stdio.h>
#include <lafs/lafs.h>
#include "internal.h"

static char *type_names[] = { "-none-",
	"InodeFile", "InodeMap", "SegmentMap", "Quota", "Orphanlist",
	"AccessTime", "7", "8", "9", "10", "11", "12", "13", "14", "15",
	"File", "Dir", "Symlink", "Device", "Pipe", "Attr"
};

void lafs_print_inode(struct lafs_ino *ino)
{
	struct lafs *fs = ino->fs;
	struct fs_md *f;
	struct inodemap_md *im;
	struct su_md *sum;
	struct file_md *fm;
	char *body;
	int bodybytes;
	int cnt;
	struct extent *ip;
	int ind;

/*	printf("inode at %p, iblock %p, data %p\n", ino, ino->iblock, ino->iblock->data);*/

	printf("Inode %lu at %llu:\n", (unsigned long)ino->inum,
	       (unsigned long long)ino->dblock->b.physaddr);
	printf(" Data Blocks  : %u\n", (unsigned)ino->cblocks);
	printf(" Index Blocks : %u\n", (unsigned)ino->ciblocks);
	printf(" Virt  Blocks : %u\n", (unsigned)(ino->cblocks+ino->ciblocks+ino->ablocks));
	printf(" Generation   : %u\n", ino->generation);
	printf(" MetaSize     : %u\n", ino->metadata_size);
	printf(" Depth        : %u\n", ino->depth);
	printf(" TruncGen     : %u\n", ino->trunc_gen);
	printf(" File Type    : %s\n", (ino->type)<22?type_names[ino->type]:"Unknown");
	if (ino->flags) {
		printf(" Flags       :");
		if (ino->flags & File_nonlogged)
			printf(" nonlogged");
		printf("\n");
	}

	body = ino->dblock->b.data + ino->metadata_size;
/*	printf(" body at %p\n", body);*/
	bodybytes = fs->blocksize - (ino->metadata_size);
	switch(ino->type) {
	case TypeInodeFile:
		f = &ino->md.fs;
		printf("  UpdateTime  : %lld\n", (unsigned long long)(f->update_time));
		printf("  Blocks      : %lld\n", (unsigned long long)(f->cblocks_used));
		//printf("  Blocks(p)   : %lld\n", (unsigned long long)(f->pblocks_used));
		printf("  BlocksAllow : %lld\n", (unsigned long long)(f->blocks_allowed));
		printf("  Creation    : %lld\n", (unsigned long long)(f->creation_age));
		printf("  Inodes      : %ld\n", (unsigned long)(f->inodes_used));
		printf("  QuotaInodes : %ld,%ld,%ld\n",
		       (unsigned long)f->quota_inums[0],
		       (unsigned long)f->quota_inums[1],
		       (unsigned long)f->quota_inums[2]);
		printf("  SS Index    : %d\n", (f->usagetable));
		printf("  Name        : %.40s\n", f->name);
		break;
	case TypeInodeMap:
		im = &ino->md.inodemap;
		printf("  Map Size    : %lu\n", (unsigned long)im->size);
		break;

	case TypeSegmentMap:
		sum = &ino->md.segmentusage;
		printf("  Table Size  : %lu\n", (unsigned long)sum->table_size);
		break;

	case TypeQuota:
	case TypeOrphanList:
	case TypeAccessTime:
		break;

	case TypeFile:
	case TypeDir:
	case TypeSymlink:
	case TypeSpecial:
	case TypeAttr:
		fm = &ino->md.file;
		printf("  Flags       : 0x%hx\n", fm->flags);
		printf("  Mode        : 0%ho\n", fm->mode);
		printf("  Uid         : %lu\n", (unsigned long)fm->uid);
		printf("  Gid         : %lu\n", (unsigned long)fm->gid);
		printf("  Treeid      : %lu\n", (unsigned long)fm->treeid);
		printf("  Creationtime: %llu\n", (unsigned long long)fm->creationtime);
		printf("  ModifyTime  : %llu\n", (unsigned long long)fm->modifytime);
		printf("  CTime       : %llu\n", (unsigned long long)fm->ctime);
		printf("  AccessTime  : %llu\n", (unsigned long long)fm->accesstime);
		printf("  Size        : %llu\n", (unsigned long long)fm->size);
		printf("  Parent      : %lu\n", (unsigned long)fm->parent);
		printf("  LinkCount   : %lu\n", (unsigned long)fm->linkcount);

		break;
	default:
		printf("---------- unknown type ----------------\n");
	}

	if (ino->depth == 0) {
		printf("Body starts:");
		for (cnt=0; cnt< 16 && cnt < bodybytes; cnt++)
			printf(" %02x", 255&body[cnt]);
		printf("\n");
	} else if (ino->depth > 1) {
		if (__le16_to_cpu(*(short*)body) != 0)
			printf("Eeek - index type %d at depth %d\n",
			       __le16_to_cpu(*(short*)body), ino->depth);
		else {
			printf(" Index Entries:\n");
			body+= 2;
			bodybytes -= 2;
			ind = 0;
			for (; bodybytes >= 10; body+=10, bodybytes -= 10) {
				unsigned char *p = (unsigned char *)body;
				u64 addr= decode48(p);
				u32 faddr=decode32(p);
				if (addr == 0)
					break;
				printf(" <%lu -> %llu>", (unsigned long)faddr,
				       (unsigned long long)addr);
				if ((ind%10)==9) printf("\n");
				ind++;
			}
			if (((ind-1)%10)!= 9) printf("\n");
		}
	} else
		switch (__le16_to_cpu(*(u16*)body)) {
		case 0:
			printf("Eeek - internal index block at level 1!!\n");
			break;
		case 1: /* indirect block */
			printf(" Indirect entries:\n");
			body+= 2;
			bodybytes -= 2;
			ind = 0;
			for (; bodybytes >= 6 ; body+= 6, bodybytes -= 6) {
				unsigned char *p = (unsigned char*)body;
				u64 addr = decode48(p);
				printf(" %llu", (unsigned long long)addr);
				if ((ind&15)==15) printf("\n");
				ind++;
			}
			printf("\n");
			break;
		case 2: /* extent block */
			printf(" Extent entries:\n");
			ip = (struct extent*)(body+2);
/*			printf("body at %p\n", ip);*/
			for (; __le16_to_cpu(ip->size) != 0
				     && ((char*)(ip+1))<(body+bodybytes) ; ip++)
				printf("  %ld..%ld at %ld\n",
				       (unsigned long)__le32_to_cpu(ip->logical),
				       (unsigned long)__le32_to_cpu(ip->logical)+
				       __le16_to_cpu(ip->size)-1,
				       (unsigned long)__le32_to_cpu(ip->phys_lo));
			break;
		default: printf("Eek - unknown index block type: %d\n",
				__le16_to_cpu(*(u16*)body));
		}
	printf("...dblock=%p iblock=%p\n", ino->dblock, ino->iblock);
//	if (ino->iblock)
//	printf("...lc[0]=%d lc[1]=%d refcnt=%d p=%p\n", iblk(ino->iblock)->lockcnt[0],
//	       iblk(ino->iblock)->lockcnt[1], ino->iblock->refcnt, ino->iblock->parent);
}
