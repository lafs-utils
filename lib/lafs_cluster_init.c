
/* lafs_cluster_init.
 * initialise the write-cluster structure.  This can identify
 * a location in the middle of a segment where write can continue
 * or no valid segment, so a new one will be allocated.
 */
#include <lafs/lafs.h>

static int seg_remainder(struct lafs *fs, struct lafs_segpos *seg)
{
	/* return the number of blocks from the start of segpos to the
	 * end of the segment.
	 * i.e. remaining rows in this table, plus remaining tables in
	 * this segment
	 */
	struct lafs_device *dv = dev_by_num(fs, seg->dev);
	int rows = dv->rows_per_table - seg->st_row;

	if (seg->dev < 0) abort();
	rows += dv->rows_per_table * (dv->tables_per_seg - seg->st_table - 1);
	return rows * dv->width;
}

static void cluster_reset(struct lafs *fs, struct lafs_cluster *wc)
{
	if (wc->seg.dev < 0) {
		wc->remaining = 0;
		wc->chead_size = 0;
		return;
	}
	wc->remaining = seg_remainder(fs, &wc->seg);
	wc->chead_blocks = 1;
	wc->remaining--;
	wc->chead_size = sizeof(struct cluster_head);
}

static void seg_setpos(struct lafs *fs, struct lafs_segpos *seg, loff_t addr)
{
	/* set seg to start at the given virtual address, and be
	 * of size 0
	 * 'addr' should always be at the start of a row, though
	 * as it might be read off storage, we need to be
	 * a little bit careful.
	 */
	loff_t offset;
	u32 row, col, table;
	struct lafs_device *dv;
	virttoseg(fs, addr, &seg->dev, &seg->num, &offset);
	dv = &fs->devs[seg->dev];
	col = offset / dv->stride;
	row = offset % dv->stride;
	table = col / dv->width;
	col = col % dv->width;
	if (col) abort(); /* FIXME return an error instead */

	seg->st_table = table;
	seg->st_row = row;

	seg->table = seg->nxt_table = seg->st_table;
	seg->row = seg->nxt_row = seg->st_row;
	seg->col = 0;
}

void lafs_cluster_init(struct lafs *fs, int cnum,
		       loff_t addr, loff_t prev, loff_t seq)
{
	struct lafs_cluster *wc = &fs->wc[cnum];

	INIT_LIST_HEAD(&wc->blocks);
	wc->prev_addr = prev;
	wc->seq = seq;
	if (addr)
		seg_setpos(fs, &wc->seg, addr);
	else
		wc->seg.dev = -1;
	cluster_reset(fs, wc);
	//fs->free_blocks = wc->remaining;
}
