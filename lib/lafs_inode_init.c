
/*
 * Fill out a block to look like an on-disk inode
 */

#include <unistd.h>
#include <stdlib.h>
#include <lafs/lafs.h>
#include <memory.h>
#include <time.h>

void lafs_inode_init(struct lafs *fs, char * buf, int type)
{
	struct lafs_ino ino;
	unsigned char *ip;

	memset(buf, 0, fs->blocksize);
	memset(&ino, 0, sizeof(ino));

	ino.generation = random();
	ino.trunc_gen = random() & 0xff;
	ino.flags = 0;
	ino.type = type;

	ino.depth = 1;
	memset(&ino.md, 0, sizeof(ino.md));
	switch(type) {
	case TypeInodeFile:
		ino.metadata_size = sizeof(struct la_inode) + sizeof(struct fs_metadata);
		ino.depth = 1; /*never depth 0 for inode files */
		ino.md.fs.update_time = time(0); /* FIXME */
		break;

	case TypeInodeMap:
		ino.metadata_size = sizeof(struct la_inode) + sizeof(struct inodemap_metadata);
		ino.md.inodemap.size = 0;
		break;

	case TypeSegmentMap:
		ino.metadata_size = sizeof(struct la_inode) + sizeof(struct su_metadata);
		ino.md.segmentusage.table_size = 0; /* FIXME */
		break;

	case TypeQuota:
		ino.metadata_size = sizeof(struct la_inode) + sizeof(struct quota_metadata);
		ino.md.quota.gracetime = 0;
		ino.md.quota.graceunits = 0;
		break;

	case TypeOrphanList:
	case TypeAccessTime:
		ino.metadata_size = sizeof(struct la_inode);
		break;

	default: /* external file */
		if (type < TypeBase) abort();
		ino.metadata_size = sizeof(struct la_inode) + sizeof(struct file_metadata);
		if (type == TypeDir)
			ino.metadata_size = sizeof(struct la_inode) + sizeof(struct dir_metadata);
		ino.md.file.flags = 0;
		ino.md.file.mode = 0600;
		ino.md.file.creationtime = time(0);
		ino.md.file.modifytime = time(0);
		ino.md.file.ctime = time(0);
		ino.md.file.accesstime = time(0);
		ino.md.file.i_accesstime = ino.md.file.accesstime;

		ino.md.file.seed = (random() & ~7) | 1;
		INIT_LIST_HEAD(&ino.md.file.dirorphan);
		break;
	}

	ip = (void*)(buf + ino.metadata_size);
	*(u16*)ip = __cpu_to_le16(IBLK_INDIRECT);

	lafs_inode_fillblock(&ino, buf);
}
