
#include <lafs/lafs.h>
#include <unistd.h>
#include <fcntl.h>

int lafs_new(struct lafs *fs, int blockbytes)
{
	int fd;
	int n;
	int blockbits;

	if (fs->blocksize != 0)
		return -1;

	if (blockbytes < 512 ||
	    blockbytes > 4096 ||
	    (blockbytes & (blockbytes-1)) != 0)
		return -1;
	blockbits = 9;
	while ((1<<blockbits) != blockbytes)
		blockbits++;


	fd = open("/dev/urandom", O_RDONLY);
	if (fd < 0)
		return -1;

	n = read(fd, fs->uuid, 16);
	close(fd);
	if (n != 16)
		return -1;

	fs->blocksize = blockbytes;
	fs->blockbits = blockbits;

	fs->seq = 1;
	fs->statesize = 1024;

	fs->wc[0].seq = 1;
	fs->wc[1].seq = 1;

	fs->youth_next = 0x8000;

	return 0;
}
