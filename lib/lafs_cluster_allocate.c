
/*
 * allocate a dirty block to a cluster so we can easily write it
 * for a flush.
 *
 * FIXME
 * For now, don't sort the blocks and assume that the cluster head
 * and segment will have adequate room for all blocks.
 * That is sufficient for mkfs
 */
#include <lafs/lafs.h>
#include <stdlib.h>
#include "internal.h"

void lafs_cluster_allocate(struct lafs_blk *b, int cnum)
{
	struct lafs *fs = b->ino->fs;
	struct lafs_cluster *wc;
	if (!(b->flags & B_Dirty))
		abort();

	if (!(b->flags & B_Index) &&
	    b->ino->type == TypeInodeFile &&
	    dblk(b)->my_inode &&
	    (dblk(b)->my_inode->iflags & I_Dirty))
		lafs_inode_fillblock(dblk(b)->my_inode, b->data);

	if ((b->flags & B_Index) &&
	    b->ino->iblock == iblk(b)) {
		/* InoIdx block - cannot write that, must write the
		 * data block instead */
		lafs_sched_blk(&b->ino->dblock->b);
		de_sched(b);
		return;
	}


	wc = &fs->wc[cnum];

	if (wc->remaining == 0) {
		lafs_cluster_flush(fs, cnum);
		lafs_new_segment(fs, cnum);
		if (!wc->remaining)
			abort();
	}
	wc->remaining--;
	list_move_tail(&b->leafs, &wc->blocks);
}
