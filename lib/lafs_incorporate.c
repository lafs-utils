
/*
 * Given an index block with some ->uninc blocks, add all the addresses
 * of those blocks into the index info.
 *
 * FIXME
 * For now we only support Indirect index in the inode as that is enough
 * for mkfs
 */

#include <lafs/lafs.h>
#include <stdlib.h>
#include <stdio.h>
#include "internal.h"

#define encode16(p,n) ({ *(p++) = (n)&255; *(p++) = ((n)>>8) & 255; })
#define encode32(p,n) ({ encode16(p,n); encode16(p, ((n)>>16)); })
#define encode48(p,n) ({ encode32(p,n); encode16(p, ((n)>>32)); })

void lafs_incorporate(struct lafs_iblk *ib)
{
	struct lafs *fs = ib->b.ino->fs;
	unsigned char *ip;
	int max_addr;

	if (ib->uninc == NULL)
		return;

	if (ib->depth != 1)
		abort();


	ip = (void*)(ib->b.ino->dblock->b.data + ib->b.ino->metadata_size);
	if (*(u16*)ip != __cpu_to_le16(IBLK_INDIRECT))
		abort();
	ip += 2;

	max_addr = fs->blocksize - ib->b.ino->metadata_size;
	max_addr -= 2; /* for type field */
	max_addr /= 6; /* 6 bytes per address */

	while (ib->uninc) {
		struct lafs_blk *b = ib->uninc;
		int addr;
		unsigned char *ip2 = ip;

		ib->uninc = b->chain;
		b->chain = NULL;
		b->flags &= ~B_Uninc;

		addr = b->fileaddr - ib->b.fileaddr;
		if (addr >= max_addr)
			abort();
		ip2 += addr * 6;
		encode48(ip2, b->physaddr);
	}
	trace(1, "incorporate %d/%lld\n", ib->b.ino->dblock->b.ino->inum,
	       (long long)ib->b.ino->dblock->b.fileaddr);
	lafs_sched_blk(&ib->b);
}
