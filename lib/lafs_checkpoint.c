
/* Force out a checkpoint
 * All scheduled blocks get incorporated and/or written
 * then we write out the new state blocks
 */

#include <stdlib.h>
#include <lafs/lafs.h>
#include <stdio.h>
#include "internal.h"

int lafs_checkpoint(struct lafs *fs)
{
	struct lafs_blk *b;

	fs->checkpointing = CHECKPOINTING | CHECKPOINT_START;

	while (!list_empty(&fs->leafs)) {
		while (!list_empty(&fs->leafs)) {
			b = list_first_entry(&fs->leafs,
					     struct lafs_blk,
					     leafs);
			trace(1, "checkpoint %p %d/%lld %s-%d %p\n", b, b->ino->inum,
			      (long long)b->fileaddr,
			      (b->flags & B_Index) ? "index":"data",
			      (b->flags & B_Index) ? iblk(b)->sched_cnt:0,
			      b->parent);
			list_del_init(&b->leafs);
			if (!(b->flags & B_Index)) {
				struct lafs_dblk *db = dblk(b);
				if (b->ino->type == TypeSegmentMap) {
					/* Allow parents to be processed,
					 * but freeze this */
					de_sched(b); b->flags |= B_Sched;
					list_add(&b->leafs, &fs->account_leafs);
					continue;
				}
				if (b->ino->type == TypeInodeFile &&
				    db->my_inode &&
				    db->my_inode->iblock &&
				    db->my_inode->iblock->sched_cnt)
					/* Wait for InoIdx block to be ready */
					continue;
			}

			if (b->flags & B_Index) {
				struct lafs_iblk *ib = iblk(b);
				if (ib->sched_cnt)
					/* Not ready yet, leave it off the list */
					continue;

				if (ib->uninc) {
					lafs_incorporate(ib);
					if (list_empty(&ib->b.leafs))
						abort();
					/* We'll pick it up next time 'round */
					continue;
				}
				if (b->flags & B_InoIdx) {
					/* InoIdx block is ready, so process
					 * the data block.
					 */
					de_sched(b);
					b->flags &= ~B_Dirty;
					b = &b->ino->dblock->b;
					b->flags |= B_Dirty;
				}
			}
			if (b->flags & B_Dirty) {
				trace(2, "...alloc\n");
				lafs_cluster_allocate(b, 0);
			} else
				de_sched(b);
		}
		lafs_cluster_flush(fs, 0);
	}
	fs->flags |= LAFS_DELAY_UPDATES;
	while (!list_empty(&fs->account_leafs)) {
		b = list_first_entry(&fs->account_leafs,
				     struct lafs_blk, leafs);
		b->flags &= ~B_Sched;
		trace(1, "Account %d/%lld\n", b->ino->inum, (long long)b->fileaddr);
		list_del_init(&b->leafs);

		lafs_sched_blk(b);
		list_del_init(&b->leafs);

		lafs_cluster_allocate(b, 0);
	}
	fs->checkpointing |= CHECKPOINT_END;
	lafs_cluster_flush(fs, 0);
	fs->flags &= ~LAFS_DELAY_UPDATES;

	lafs_write_state(fs);
	lafs_segment_apply_delayed(fs);
	return 0;
}
