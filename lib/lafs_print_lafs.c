#include <uuid/uuid.h>
#include <lafs/lafs.h>
#include <stdio.h>
#include "internal.h"

void lafs_print_lafs(struct lafs *fs)
{
	char uuidstr[37];
	int s;
	struct lafs_snapshot *ss;

	printf("LaFS State:\n");
	uuid_unparse(fs->uuid, uuidstr);
	printf(" UUID       : %s\n", uuidstr);
	printf(" Seq        : %d\n", (int)fs->seq);
	printf(" Devices    : %d\n", fs->devices);
	printf(" Loaded devs: %d\n", fs->loaded_devs);
	printf(" BlockSize  : %d\n", fs->blocksize);
	printf(" StateSize  : %d\n", fs->statesize);
	printf(" MaxSegsize : %d\n", fs->max_segment);
	printf(" CheckPoint : %llu\n", (unsigned long long)fs->checkpoint_cluster);
	printf(" NextYouth  : %d\n", fs->youth_next);
	for (s= 0, ss = &fs->ss; ss ; ss = ss->next)
		printf(" Root[%d]    : %llu\n", s, (unsigned long long)ss->root_addr);
}
