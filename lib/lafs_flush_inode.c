/*
 * Flush all dirty blocks in an inode to write cluster
 */
#include <lafs/lafs.h>
#include "internal.h"

void lafs_flush_inode(struct lafs_ino *inode)
{
	struct lafs_blk *blk;
	struct list_head *tmp;

	list_for_each_entry_safe(blk, tmp, &inode->dirty, leafs) {
		if (!(blk->flags & B_Dirty))
			list_del_init(&blk->leafs);

		if ((blk->flags & B_Dirty) &&
		    !(blk->flags & B_Sched))
			lafs_cluster_allocate(blk, 0);
	}
}

void lafs_flush(struct lafs *lafs)
{
	struct lafs_ino *ino;
	struct list_head *tmp;

	list_for_each_entry_safe(ino, tmp, &lafs->dirty_inodes, dirty_inodes) {
		lafs_flush_inode(ino);
		if (list_empty(&ino->dirty))
			list_del_init(&ino->dirty_inodes);
	}
}
	
