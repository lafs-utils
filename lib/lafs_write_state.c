
/*
 * Write the state blocks twice to each device.
 */

#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <unistd.h>
#include <lafs/lafs.h>
#include <memory.h>
#include "internal.h"

int lafs_write_state(struct lafs *fs)
{
	char buf[4096];
	struct lafs_state *st = (void*)buf;
	struct lafs_device *dev;

	memset(buf, 0, 4096);

	fs->seq++;

	memcpy(st->idtag, "LaFS-State-Block", 16);
	memcpy(st->uuid, fs->uuid, 16);
	st->version = __cpu_to_le32(LAFS_STATE_VERS);

	st->seq = __cpu_to_le32(fs->seq);
	st->nextyouth = __cpu_to_le16(fs->youth_next);
	st->checkpointcluster = __cpu_to_le64(fs->checkpoint_cluster);
	st->root_inodes[0] = __cpu_to_le64(fs->ss.root_addr);
	st->maxsnapshot = 1;
	st->devices = __cpu_to_le32(fs->devices);

	st->checksum = crc32(0, (uint32_t *)buf, fs->statesize);

	for (dev = fs->devs ; dev ; dev = dev->next) {
		int n;
		for (n = (fs->seq & 1); n < 4 ; n += 2) {
			lseek64(dev->fd, dev->stateaddr[n], SEEK_SET);
			write(dev->fd, buf, fs->statesize);
		}
	}
	return 0;
}
