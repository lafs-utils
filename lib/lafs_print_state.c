#include <uuid/uuid.h>
#include <lafs/lafs.h>
#include <stdio.h>
#include "internal.h"

void lafs_print_state(struct lafs_state *state, int size)
{
	char uuidstr[37];
	u32 crc, crc2;
	int l;
	int maxss;

	printf("Stateblock:\n");
	printf(" IDtag      : %.16s\n", state->idtag);
	printf(" Version    : %d\n", (int)__le32_to_cpu(state->version));
	uuid_unparse(state->uuid, uuidstr);
	printf(" UUID       : %s\n", uuidstr);
	printf(" Checksum   : %08x", (int) __le32_to_cpu(state->checksum));
	crc = state->checksum;
	state->checksum = 0;
	crc2 = crc32(0, (uint32_t*)state, size);
	state->checksum = crc;
	if (crc2 == crc)
		printf(" (correct)\n");
	else
		printf(" (expected %08x)\n", (unsigned int)__le32_to_cpu(crc2));
	printf(" Seq        : %d\n", (int)__le32_to_cpu(state->seq));
	printf(" AltSeq     : %d\n", (int)__le32_to_cpu(state->alt_seq));
	printf(" Devices    : %d\n", (int)__le32_to_cpu(state->devices));
	printf(" MaxSnap    : %d\n", (int)__le32_to_cpu(state->maxsnapshot));
	printf(" NextYouth  : %d\n", (int)__le16_to_cpu(state->nextyouth));
	if (__le16_to_cpu(state->nonlog_dev) < 0xFFFF) {
		printf(" NonLogSeg  : %d\n", (int)__le32_to_cpu(state->nonlog_segment));
		printf(" NonLogDev  : %d\n", (int)__le16_to_cpu(state->nonlog_dev));
		printf(" NonLogOff  : %d\n", (int)__le16_to_cpu(state->nonlog_offset));
	}

	printf(" CheckPoint : %llu\n",
	       (unsigned long long)__le64_to_cpu(state->checkpointcluster));
	maxss = __le32_to_cpu(state->maxsnapshot);
	if (maxss > (size - sizeof(*state)) / 4)
		maxss = (size - sizeof(*state)) / 4;
	for (l=0; l < maxss; l++)
		printf(" Root[%d]    : %llu\n", l+1,
		       (unsigned long long)__le64_to_cpu(state->root_inodes[l]));

	printf("\n");
}

