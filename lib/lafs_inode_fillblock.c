
/*
 * Fill a data buffer with info to look like an on-disk inode
 */

#include <stdlib.h>
#include <lafs/lafs.h>
#include <memory.h>
#include <stdio.h>
#include "internal.h"

void lafs_inode_fillblock(struct lafs_ino *ino, char *buf)
{
	struct la_inode *lino = (void*)buf;

	lino->data_blocks = __cpu_to_le32(ino->cblocks);
	lino->index_blocks = __cpu_to_le32(ino->ciblocks);
	lino->generation = __cpu_to_le16(ino->generation);
	lino->metadata_size = __cpu_to_le16(ino->metadata_size);
	lino->depth = ino->depth;
	lino->trunc_gen = ino->trunc_gen;
	lino->filetype = ino->type;
	lino->flags = ino->flags;

	trace(1, "FILL inode (type=%d)\n", ino->type);
	switch(ino->type) {
	case TypeInodeFile:
	{
		struct fs_metadata *sfs = &lino->metadata[0].fs;
		struct fs_md *mfs = &ino->md.fs;
		sfs->update_time = __cpu_to_le64(mfs->update_time);
		sfs->blocks_used = __cpu_to_le64(mfs->cblocks_used);
		sfs->blocks_allowed = __cpu_to_le64(mfs->blocks_allowed);
		sfs->creation_age = __cpu_to_le64(mfs->creation_age);
		sfs->inodes_used = __cpu_to_le32(mfs->inodes_used);
		sfs->parent = __cpu_to_le32(mfs->parent);
		sfs->snapshot_usage_table = __cpu_to_le16(mfs->usagetable);
		sfs->quota_inodes[0] = __cpu_to_le32(mfs->quota_inums[0]);
		sfs->quota_inodes[1] = __cpu_to_le32(mfs->quota_inums[1]);
		sfs->quota_inodes[2] = __cpu_to_le32(mfs->quota_inums[2]);
		/* FIXME do something with name */
		break;
	}
	case TypeInodeMap:
	{
		struct inodemap_metadata *sim = &lino->metadata[0].inodemap;
		struct inodemap_md *mim = &ino->md.inodemap;
		sim->size = __cpu_to_le32(mim->size);
		break;
	}
	case TypeSegmentMap:
	{
		struct su_metadata *s = &lino->metadata[0].segmentusage;
		struct su_md *m = &ino->md.segmentusage;
		s->table_size = __cpu_to_le32(m->table_size);
		break;
	}
	case TypeQuota:
	{
		struct quota_metadata *s = &lino->metadata[0].quota;
		struct quota_md *m = &ino->md.quota;
		s->gracetime = __cpu_to_le32(m->gracetime);
		s->graceunits = __cpu_to_le32(m->graceunits);
		break;
	}
	case TypeOrphanList:
	case TypeAccessTime:
		break;
	default: /* external-file */
	{
		struct file_metadata *sfl = & lino->metadata[0].file;
		struct file_md *mfl = & ino->md.file;
		struct dir_metadata *sdr = & lino->metadata[0].dir;
		struct special_metadata *ssp = & lino->metadata[0].special;

		if (ino->type < TypeBase) abort();
		sfl->flags = __cpu_to_le16(mfl->flags);
		sfl->mode = __cpu_to_le16(mfl->mode);
		sfl->userid = __cpu_to_le32(mfl->uid);
		sfl->groupid = __cpu_to_le32(mfl->gid);
		sfl->treeid = __cpu_to_le32(mfl->treeid);
		sfl->creationtime = __cpu_to_le64(mfl->creationtime);
		sfl->modifytime = __cpu_to_le64(mfl->modifytime);
		sfl->ctime = __cpu_to_le64(mfl->ctime);
		sfl->accesstime = __cpu_to_le64(mfl->i_accesstime);
		/* FIXME should we zero the accesstime file at this point? */
		sfl->size = __cpu_to_le64(mfl->size);
		sfl->parent = __cpu_to_le32(mfl->parent);
		sfl->linkcount = __cpu_to_le32(mfl->linkcount);
		sfl->attrinode = 0;

		if (ino->type == TypeDir)
			sdr->hash_seed = __cpu_to_le32(mfl->seed);
		if (ino->type == TypeSpecial) {
			ssp->major = __cpu_to_le32(mfl->major);
			ssp->minor = __cpu_to_le32(mfl->minor);
		}

		break;
	}
	}
}
