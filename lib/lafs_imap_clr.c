
/*
 * Mark an inode number as being not-used in the inode map.
 */

#include <lafs/lafs.h>
#include "internal.h"

int lafs_imap_clr(struct lafs_ino *ino, int inum)
{
	struct lafs *fs = ino->fs;
	struct lafs_dblk *db;
	int blknum = (inum / fs->blocksize) / 8;

	if (ino->type != TypeInodeMap)
		return -1;
	if (blknum >= ino->md.inodemap.size)
		return 0;

	db = lafs_dblk(ino, blknum);
	lafs_load_dblk(db);
	inum -= blknum * fs->blocksize * 8;
	set_c_bit(inum, (unsigned char *)db->b.data);
	/* FIXME if block is now empty, possibly contract file */
	lafs_sched_blk(&db->b);
	return 0;
}
