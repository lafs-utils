
/*
 * load the content of this dblk from the filesystem, or maybe
 * from the inode
 */
#include <lafs/lafs.h>
#include <memory.h>

int lafs_load_dblk(struct lafs_dblk *db)
{
	struct lafs_ino *ino = db->b.ino;
	struct lafs *fs = ino->fs;
	struct lafs_dblk *inodb;
	int offset;

	if (db->b.flags & B_Valid)
		return 0;

	lafs_find_dblk(db);

	db->b.flags |= B_Valid;
	if (db->b.physaddr)
		return lafs_read_virtual(fs, db->b.data,
					 db->b.physaddr);

	memset(db->b.data, 0, fs->blocksize);
	if (ino->depth > 0)
		return 0;

	/* content is in the inode dblock */
	inodb = ino->dblock;
	offset = ino->metadata_size;
	memcpy(db->b.data, inodb->b.data + offset, fs->blocksize - offset);
	return 0;
}
