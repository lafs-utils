/*
 * find or create a dblk for a given offset in a file,
 * and return it.
 */

#include <lafs/lafs.h>
#include <talloc.h>
#include <memory.h>


struct lafs_dblk *lafs_dblk(struct lafs_ino *ino, loff_t bnum)
{
	struct lafs_dblk *db;
	struct lafs *fs = ino->fs;
	int hash = (long)ino;

	hash ^= bnum;
	hash ^= hash >> 10;
	hash &= (1<<HASH_BITS)-1;

	list_for_each_entry(db, &fs->htable[hash], b.hash) {
		if (db->b.flags & B_Index)
			continue;
		if (db->b.ino != ino ||
		    db->b.fileaddr != bnum)
			continue;
		return db;
	}

	/* Not in the table, so need to create it */
	db = talloc(ino, struct lafs_dblk);
	memset(db, 0, sizeof(*db));

	db->b.data = talloc_size(db, fs->blocksize);
	db->b.fileaddr = bnum;
	db->b.ino = ino;
	list_add(&db->b.hash, &fs->htable[hash]);
	INIT_LIST_HEAD(&db->b.siblings);
	INIT_LIST_HEAD(&db->b.leafs);
	return db;
}
