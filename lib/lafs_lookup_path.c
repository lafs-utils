#include <lafs/lafs.h>

struct lafs_ino *lafs_lookup_path(struct lafs_ino *root, struct lafs_ino *cwd,
				  char *path, char **remainder)
{
	if (!path)
		return cwd;
	if (*path == '/') {
		cwd = root;
		while (*path == '/')
			path++;
	}

	while (cwd && *path) {
		char *p = path;
		int len;
		u32 inum = 0;
		while (*path && *path != '/')
			path++;
		len = path - p;
		while (*path == '/')
			path++;

		if (cwd->type == TypeDir)
			inum = lafs_dir_lookup(cwd, p, len);
		if (!inum) {
			if (!remainder)
				return NULL;
			*remainder = p;
			return cwd;
		}
		cwd = lafs_get_inode(cwd->filesys, inum);
	}
	return cwd;
}
