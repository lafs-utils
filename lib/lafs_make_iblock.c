
/*
 * Allocate an iblock for the given inode
 */
#include <lafs/lafs.h>
#include <memory.h>
#include <talloc.h>
#include <lafs/list.h>

void lafs_make_iblock(struct lafs_ino *ino)
{
	struct lafs_iblk *ib;
	if (ino->iblock)
		return;

	ib = talloc(ino, struct lafs_iblk);
	memset(ib, 0, sizeof(*ib));

	ib->depth = ino->depth;
	INIT_LIST_HEAD(&ib->children);
	ib->b.flags |= B_Index | B_InoIdx;
	ib->b.ino = ino;
	INIT_LIST_HEAD(&ib->b.hash);
	ib->b.parent = ino->dblock->b.parent;
	INIT_LIST_HEAD(&ib->b.siblings);
	INIT_LIST_HEAD(&ib->b.leafs);

	ino->iblock = ib;
}
