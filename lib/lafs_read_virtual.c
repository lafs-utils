/*
 * read a block given a virtual address
 */
#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <unistd.h>
#include <lafs/lafs.h>


int lafs_read_virtual(struct lafs *fs, char *buf, loff_t addr)
{
	struct lafs_device *d = lafs_dev_find(fs, addr);
	int n;

	if (!d)
		return -1;
	lseek64(d->fd,
		(addr - d->start) * fs->blocksize + d->segment_offset,
		SEEK_SET);
	n = read(d->fd, buf, fs->blocksize);

	if (n == fs->blocksize)
		return 0;
	return -1;
}
