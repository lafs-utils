#include <string.h>
#include <lafs/lafs.h>
#include "internal.h"

static inline int space_needed(int len, int chainoffset, int psz)
{
	int space;
	space = len + (chainoffset > 255 ? 4 : chainoffset > 1 ? 1 : 0);
	space += offsetof(struct dirpiece, name);
	space = DIV_ROUND_UP(space, 1<<psz);
	return space;
}


int lafs_dir_add(struct lafs_ino *dir, char *name, u32 inum, int type)
{
	/* lookup a name and return ino number, or 0 if not found */
	struct lafs_dblk *db, *new;
	u32 hash, newhash;
	int chainoffset = 0;
	int len = strlen(name);
	int rv;
	struct dirheader *dh;
	char *n1, *n2;

	hash = lafs_hash_name(dir->md.file.seed, len, name);

	while (1) {
		u32 bnum;
		int found;
		u8 piece;
		struct dir_ent de;
		bnum = lafs_find_next(dir, hash+1);
		if (bnum == LAFS_NOBLOCK)
			bnum = 0;

		db = lafs_dblk(dir, bnum);
		if (!db ||
		    lafs_load_dblk(db))
			return 0;

		found = lafs_dir_find(db->b.data, dir->fs->blockbits-8,
				      dir->md.file.seed,
				      hash, &piece);
		if (found) {
			lafs_dir_extract(db->b.data, dir->fs->blockbits-8,
					 &de, piece, NULL);
			if (de.target &&
			    de.nlen == len &&
			    strncmp(de.name, name, len) == 0)
				return 0; /* already exists */
			if (de.target) {
				/* chain forwards */
				hash++;
				chainoffset++;
				continue;
			}
		}
		/* This hash value is free. */
		break;
	}
	rv = lafs_dir_add_ent(db->b.data, dir->fs->blockbits-8,
			      name, len, inum, type,
			      dir->md.file.seed,
			      hash, chainoffset);
	BUG_ON(rv < 0);
	if (rv == 1) {
		lafs_sched_blk(&db->b);
		return 1;
	}
	/* didn't fit - need to split */
	dh = (struct dirheader*)(db->b.data);
	if (dh->freepieces >= space_needed(len, chainoffset,
					   dir->fs->blockbits-8)) {
		char *tmp = malloc(dir->fs->blocksize);
		lafs_dir_repack(db->b.data, dir->fs->blockbits-8,
				tmp, dir->md.file.seed, 0);
		rv = lafs_dir_add_ent(tmp, dir->fs->blockbits-8,
				      name, len, inum, type,
				      dir->md.file.seed,
				      hash, chainoffset);
		if (rv == 1) {
			memcpy(db->b.data, tmp, dir->fs->blocksize);
			free(tmp);
			lafs_sched_blk(&db->b);
			return 1;
		}
		free(tmp);
	}
	/* Really doesn't fit, need to split */
	n1 = malloc(dir->fs->blocksize);
	n2 = malloc(dir->fs->blocksize);
	lafs_dir_split(db->b.data, dir->fs->blockbits-8, n1, n2,
		       name, inum, type, &newhash,
		       dir->md.file.seed, hash, chainoffset);
	memcpy(db->b.data, n2, dir->fs->blocksize);
	new = lafs_dblk(dir, newhash+1);
	memcpy(new->b.data, n1, dir->fs->blocksize);
	free(n1);
	free(n2);
	lafs_sched_blk(&db->b);
	lafs_sched_blk(&new->b);
	return 1;
}
