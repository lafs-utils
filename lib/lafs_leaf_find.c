#include <lafs/lafs.h>

struct lafs_iblk *lafs_leaf_find(struct lafs_ino *inode,
				 u32 addr, int adopt, u32 *next)
{
	/* Find the leaf index block which refers to this address (if any do)
	 */
#if 0
	struct lafs_iblk *ib, *ib2;
	int depth;
	u64 iphys;
	u32 iaddr;
#endif
	if (next)
		*next = LAFS_NOBLOCK;

	/* now check out addressing from the inode */
	if (inode == NULL || inode->depth <= 1) {
		/* Data or leaf indexing is in inode.
		 */
		lafs_make_iblock(inode);
		return inode->iblock;
	}

#if 0
	/* depth is >= 2, so the inode contains indexes.
	 * find the appropriate index, find the relevant index block,
	 * load it, and see what's there... */
	depth = inode->depth - 1;
	iphys = index_lookup((unsigned char*)inode->dblock->data + inode->metadata_size,
			     fs->blocksize - inode->metadata_size,
			     addr,
			     &iaddr, next);

	/* FIXME if there is corruption, iphys could be zero which will
	 * cause problems soon
	 */
	if (iphys==0) abort();

	if (iaddr > 1000000) abort();
	ib = block_ifind(fs, inode, iaddr, depth, iphys);

	if (! (ib->flags & B_Linked)) {
		/* need to check for peers */
		ib2 = peer_find(fs, inode->filesys, inode, iaddr, inode->depth-1,iphys);
		if (ib2)
			list_add(&ib->peers, &ib2->peers);
		ib->flags |= B_Linked;
	}
	if (adopt) {
		inode_make_iblock(fs, inode, adopt);
		block_adopt(ib, inode->iblock);
	}
	load_block(fs, ib);
	/* this block may not be the parent we want.  It may have been split already.
	 * If it has, other blocks will be immediately afterwards in the sibling
	 * list. So we walk down the sibling list until we find a block that
	 * is before ib or after addr, and we keep the block before that.
	 */
	if (ib->parent)
	while(!list_empty(&ib->siblings) /* HACK FIXME */) {
		struct block *nxt;

		if (ib->siblings.next == &iblk(ib->parent)->children)
			break;
		nxt = list_entry(ib->siblings.next, struct block, siblings);
		if (nxt->fileaddr < ib->fileaddr || nxt->fileaddr > addr)
			break;
		getref(nxt); putref(ib);
		ib = nxt;
	}

	while (depth > 1) {
		depth--;
		/* ib contains indexes, so repeat what we did above */
		iphys = index_lookup((unsigned char*)ib->data, fs->blocksize, addr, &iaddr, next);
		ib2 = block_ifind(fs, inode, iaddr, depth, iphys);
		if (!(ib2->flags & B_Linked)) {
			struct block *b2 = peer_find(fs, inode->filesys,
						     inode, iaddr,
						     depth, iphys);
			if (b2)
				list_add(&ib2->peers, &b2->peers);
			ib2->flags |= B_Linked;
		}
		load_block(fs, ib2);
		while(ib2->parent) {
			struct block *nxt;
			if (ib2->siblings.next == &iblk(ib2->parent)->children)
				break;
			nxt = list_entry(ib2->siblings.next, struct block, siblings);
			if (nxt->fileaddr < ib2->fileaddr || nxt->fileaddr > addr)
				break;
			getref(nxt);
			putref(ib2);
			ib2 = nxt;
		}
		if(adopt) {
			block_lock(ib); /* FIXME error check */
			block_adopt(ib2, ib);
		}
		putref(ib);
		ib = ib2;
	}
	/* ib contains leaf indexing, either indirect or extent
	 * see above
	 */
	return ib;
#else
	return NULL;
#endif
}
