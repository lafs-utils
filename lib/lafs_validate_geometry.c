/* lafs_validate_geometry.
 * When adding a device to a LaFS we need to check the geometry
 * parameters are all valid and consistent, and we need to set
 * defaults.
 * That is done here and used by mkfs and 'lafs'.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <lafs/lafs.h>

int is_pow2(long num)
{
	return (num & (num-1)) == 0;
}


char *lafs_validate_geometry(long *block_bytes,
			     long *segment_bytes,
			     long *stride_bytes,
			     int *width,
			     long long device_bytes)
{
	char *error = NULL;

	if (*block_bytes == 0)
		*block_bytes = 1024;

	if (*block_bytes < 512 || *block_bytes > 4096 ||
	    !is_pow2(*block_bytes)) {
		asprintf(&error, "block size %ld is illegal - must be "
			 "power of 2 in range 512..4096",
			 *block_bytes);
		return error;
	}

	if (*width == 0)
		*width = 1;
	if (*width < 1 || *width > 128) {
		asprintf(&error, "width %d is illegal - must be in range 1..128",
			*width);
		return error;
	}

	if (*stride_bytes == 0)
		*stride_bytes = *block_bytes;

	if (*stride_bytes < 0 || (*stride_bytes % *block_bytes) != 0) {
		asprintf(&error, "stride %ld is illegal - must be "
			 "a multiple of block size",
			 *stride_bytes);
		return error;
	}

	/* segment size must be a multiple of block size and of width.
	 * It must either be a multiple or a sub-multiple of stride size
	 */
	if (*segment_bytes == 0) {
		/* choose maximum size ?? */
		long seg;
		long blocks = 65536 / *width;
		blocks *= *width;
		seg = *block_bytes * blocks;
		if (seg * 17 > device_bytes) {
			blocks = device_bytes / 16 / *block_bytes / *width;
			blocks *= *width;
			seg = *block_bytes * blocks;
		}
		if (*stride_bytes < seg) {
			seg /= *stride_bytes;
			seg *= *stride_bytes;
		} else {
			if ((*stride_bytes % seg) != 0) {
				asprintf(&error, "explicit segment size must be "
					 "given with large stride");
				return error;
			}
		}
		*segment_bytes = seg;
	}

	if (*segment_bytes * 8 > device_bytes)
		asprintf(&error, "segment size too large for device.");
	else if (*segment_bytes < *block_bytes * 8)
		asprintf(&error, "segment must hold at least 8 blocks.");
	else if (*segment_bytes % *block_bytes)
		asprintf(&error, "segment size must be a multiple of block size");
	else if (*segment_bytes % *width)
		asprintf(&error, "segment size must be a multiple of width");
	else if (*segment_bytes > *stride_bytes &&
		 (*segment_bytes % *stride_bytes))
		 asprintf(&error, "segment size must be a multiple of stride size");
	else if (*segment_bytes < *stride_bytes &&
		 (*stride_bytes % *segment_bytes))
		asprintf(&error, "segment size must be a divisor of stride size");

	return error;
}
