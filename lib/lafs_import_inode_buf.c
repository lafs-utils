
/*
 * create a new inode using the disk-image info in the given
 * buf.  Inode is in the inodefile 'parent'.
 * This should not be used directly.  It is only separate from
 * lafs_import_inode so that lafs_get_itable can break a loop.
 */

#include <lafs/lafs.h>
#include <talloc.h>
#include <memory.h>

static int inode_load(struct lafs_ino *ino, struct la_inode *lai);


struct lafs_ino *lafs_import_inode_buf(struct lafs *fs,
				       char *buf, int inum,
				       struct lafs_ino *parent)
{
	struct lafs_ino *ino;

	ino = talloc((void*)parent ?: (void*)fs, struct lafs_ino);

	memset(ino, 0, sizeof(*ino));

	ino->fs = fs;
	ino->inum = inum;
	ino->filesys = parent;
	INIT_LIST_HEAD(&ino->dirty);
	INIT_LIST_HEAD(&ino->dirty_inodes);

	if (inode_load(ino, (struct la_inode *)buf))
		/* FIXME callers of this should reparent it to themselves. */
		return ino;
	talloc_free(ino);
	return NULL;
}


static int inode_load(struct lafs_ino *ino, struct la_inode *lai)
{
	if (lai->filetype == 0) {
		ino->type = 0;
		return 0; /* no inode here */
	}

	ino->cblocks = __le32_to_cpu(lai->data_blocks);
	ino->ciblocks = __le32_to_cpu(lai->index_blocks);
	ino->generation = __le16_to_cpu(lai->generation);
	ino->trunc_gen = lai->trunc_gen;
	ino->flags = lai->flags;
	ino->type = lai->filetype;
	ino->metadata_size = __le16_to_cpu(lai->metadata_size);
	ino->depth = lai->depth;

	switch(ino->type) {
	case TypeInodeFile:
	{
		struct fs_md *i = &ino->md.fs;
		struct fs_metadata *l = &lai->metadata[0].fs;
		i->usagetable = __le16_to_cpu(l->snapshot_usage_table);
		i->update_time = __le64_to_cpu(l->update_time);
		i->cblocks_used = __le64_to_cpu(l->blocks_used);
		i->blocks_allowed = __le64_to_cpu(l->blocks_allowed);
		i->creation_age = __le64_to_cpu(l->creation_age);
		i->inodes_used = __le32_to_cpu(l->inodes_used);
		i->parent = __le32_to_cpu(l->parent);
		i->quota_inums[0] = __le32_to_cpu(l->quota_inodes[0]);
		i->quota_inums[1] = __le32_to_cpu(l->quota_inodes[1]);
		i->quota_inums[2] = __le32_to_cpu(l->quota_inodes[2]);
		i->quota_inodes[0] = i->quota_inodes[1] = i->quota_inodes[2] = NULL;
		i->name = NULL; /* FIXME */
		break;
	}
	case TypeInodeMap:
	{
		struct inodemap_md *m = &ino->md.inodemap;
		struct inodemap_metadata *s = &lai->metadata[0].inodemap;
		m->size = __le32_to_cpu(s->size);
		break;
	}
	case TypeSegmentMap:
	{
		struct su_md *m = &ino->md.segmentusage;
		struct su_metadata *s = &lai->metadata[0].segmentusage;
		m->table_size = __le32_to_cpu(s->table_size);
		break;
	}
	case TypeQuota:
	{
		struct quota_md *m = &ino->md.quota;
		struct quota_metadata *s = &lai->metadata[0].quota;
		m->gracetime = __le32_to_cpu(s->gracetime);
		m->graceunits = __le32_to_cpu(s->graceunits);
		break;
	}
	case TypeOrphanList:
	case TypeAccessTime:
		break;
	default: /* TypeBase or larger */
	{
		struct file_md *i = &ino->md.file;
		struct file_metadata *l = &lai->metadata[0].file;
		struct dir_metadata *d = &lai->metadata[0].dir;
		struct special_metadata *s = &lai->metadata[0].special;

		if (ino->type < TypeBase) abort();
		i->flags = __le16_to_cpu(l->flags);
		i->mode = __le16_to_cpu(l->mode);
		i->uid = __le32_to_cpu(l->userid);
		i->gid = __le32_to_cpu(l->groupid);
		i->treeid = __le32_to_cpu(l->treeid);
		i->creationtime = __le64_to_cpu(l->creationtime);
		i->modifytime = __le64_to_cpu(l->modifytime);
		i->ctime = __le64_to_cpu(l->ctime);
		i->i_accesstime = __le64_to_cpu(l->accesstime);
		i->accesstime = i->i_accesstime; /* FIXME load from accesstime file */
		i->size = __le64_to_cpu(l->size);
		i->parent = __le32_to_cpu(l->parent);
		i->linkcount = __le32_to_cpu(l->linkcount);

		if (ino->type == TypeDir)
			i->seed = __le32_to_cpu(d->hash_seed);
		if (ino->type == TypeSpecial) {
			i->major = __le32_to_cpu(s->major);
			i->minor = __le32_to_cpu(s->minor);
		}

		INIT_LIST_HEAD(&i->dirorphan);
		break;
	}
	}
	/* FIXME should I sanity-check anything? */
	return 1;

}
