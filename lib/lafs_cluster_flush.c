
/*
 * Flush out the given cluster with a cluster head.
 * We "know" there is room in the cluster head and
 * in the segment
 */
#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64

#include <lafs/lafs.h>
#include <unistd.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include "internal.h"

/*-----------------------------------------------------------------------
 * A segment is divided up in a slightly complicated way into
 * tables, rows and columns.  This allows us to align writes with
 * stripes in a raid4 array or similar.
 * So we have some support routines to help track our way around
 * the segment.
 *
 * A write cluster always comprises a whole number of rows - it is padded
 * with zeros if necessary.
 * However block addressing follow columns, so the blocks written to
 * a cluster may well not be contiguously addressed.
 *
 * There are 'width' blocks (or columns) in a row, and either:
 *  - segment_stride rows in a table, if segment_stride < segment_size or
 *  - one table per segment, when segment_stride > segment_size.
 * These are calculated and summarised in rows_per_table and tables_per_seg
 * in the 'struct lafs_device' structure.
 *
 * A 'seg_pos' records the current segment and where we are in it.
 * It has:
 *   dev,num  to identify the segment
 *   st_table,st_row to identify where the cluster starts.
 *   nxt_table,nxt_row to identify where the cluster ends.
 *   table,row,col to identify where the next block will be found
 */

static int seg_remainder(struct lafs *fs, struct lafs_segpos *seg)
{
	/* return the number of blocks from the start of segpos to the
	 * end of the segment.
	 * i.e. remaining rows in this table, plus remaining tables in
	 * this segment
	 */
	struct lafs_device *dv = dev_by_num(fs, seg->dev);
	int rows = dv->rows_per_table - seg->st_row;

	if (seg->dev < 0) abort();
	rows += dv->rows_per_table * (dv->tables_per_seg - seg->st_table - 1);
	return rows * dv->width;
}

#if 0
static void seg_step(struct lafs *fs, struct lafs_segpos *seg)
{
	/* reposition this segpos to be immediately after it's current end
	 * and make the 'current' point be the start.
	 * Size will be empty
	 */
	seg->st_table = seg->nxt_table;
	seg->st_row = seg->nxt_row;
	seg->table = seg->st_table;
	seg->row = seg->st_row;
	seg->col = 0;
}
#endif

static u32 seg_setsize(struct lafs *fs, struct lafs_segpos *seg, u32 size)
{
	/* move the 'nxt' table/row to be 'size' blocks beyond
	 * current start. size will be rounded up to a multiple
	 * of width.
	 */
	struct lafs_device *dv = dev_by_num(fs, seg->dev);
	u32 rv;
	int rows;

	if (seg->dev < 0) abort();
	rows = (size + dv->width - 1) / dv->width;
	rv = rows * dv->width;
	rows += seg->st_row;
	seg->nxt_table = seg->st_table + rows / dv->rows_per_table;
	seg->nxt_row = rows % dv->rows_per_table;
	return rv;
}

static u64 seg_addr(struct lafs *fs, struct lafs_segpos *seg)
{
	/* Return the virtual address of the blocks pointed
	 * to by 'seg'.
	 */
	struct lafs_device *dv = dev_by_num(fs, seg->dev);
	u64 addr;

	if (seg->dev < 0)
		/* Setting 'next' address for last cluster in
		 * a cleaner segment */
		return 0;
	addr = seg->col * dv->stride;
	addr += seg->row;
	addr += seg->table * dv->rows_per_table;
	addr += seg->num * dv->segment_stride;
	addr += dv->start;
	return addr;
}

static u64 seg_next(struct lafs *fs, struct lafs_segpos *seg)
{
	/* step forward one block, returning the address of
	 * the block stepped over
	 */
	struct lafs_device *dv = dev_by_num(fs, seg->dev);
	u64 addr = seg_addr(fs, seg);

	/* now step forward in column or table or seg */
	seg->col++;
	if (seg->col >= dv->width) {
		seg->col = 0;
		seg->row++;
		if (seg->row >= dv->rows_per_table) {
			seg->row = 0;
			seg->table++;
		}
	}
	return addr;
}

/*-------------------------------------------------------------------------
 * Cluster head building.
 * We build the cluster head bit by bit as we find blocks
 * in the list.  These routines help.
 */

static void cluster_addhead(struct lafs_cluster *wc, struct lafs_ino *ino,
			    int cnum,
			    struct group_head **headstart)
{
	struct group_head *gh = (void*)((char *)wc->chead +
					wc->chead_size);
	u16 tnf;

	*headstart = gh;
	gh->inum = __cpu_to_le32(ino->inum);
	gh->fsnum = __cpu_to_le32(ino->filesys->inum);
	gh->timestamp = __cpu_to_le64(0);
	tnf = ((ino->generation<<8) | (ino->trunc_gen & 0xff))
		& 0x7fff;
	if (cnum)
		tnf |= 0x8000;
	gh->truncatenum_and_flag = __cpu_to_le16(tnf);
	wc->chead_size += sizeof(struct group_head);
}

static void cluster_closehead(struct lafs_cluster *wc,
				     struct group_head *headstart)
{
	int size = wc->chead_size - (((char *)headstart) - (char *)wc->chead);

	headstart->group_size_words = size / 4;
}

#if 0
static void cluster_addmini(struct lafs_cluster *wc, u32 addr, int offset,
				   int size, const char *data,
				   int size2, const char *data2)
{
	/* if size2 !=0, then only
	 * (size-size2) is at 'data' and the rest is at 'data2'
	 */
	struct miniblock *mb = ((struct miniblock *)
				((char *)wc->chead + wc->chead_size));

	mb->block_num = __cpu_to_le32(addr);
	mb->block_offset = __cpu_to_le16(offset);
	mb->length = __cpu_to_le16(size + DescMiniOffset);
	wc->chead_size += sizeof(struct miniblock);
	memcpy(mb->data, data, size-size2);
	if (size2)
		memcpy(mb->data + size-size2, data2, size2);
	memset(mb->data+size, 0, (-size)&3);
	wc->chead_size += ROUND_UP(size);
}
#endif

static void cluster_adddesc(struct lafs_cluster *wc, struct lafs_blk *blk,
				   struct descriptor **desc_start)
{
	struct descriptor *dh = (struct descriptor *)((char *)wc->chead +
						      wc->chead_size);
	*desc_start = dh;
	dh->block_num = __cpu_to_le32(blk->fileaddr);
	dh->block_cnt = __cpu_to_le32(0);
	dh->block_bytes = 0;
	if (blk->flags && B_Index)
		dh->block_bytes = DescIndex;
	wc->chead_size += sizeof(struct descriptor);
}

static void cluster_incdesc(struct lafs_cluster *wc, struct descriptor *desc_start,
				   struct lafs_blk *b, int blksz)
{
	desc_start->block_cnt =
		__cpu_to_le32(__le32_to_cpu(desc_start->block_cnt)+1);
	if (!(b->flags & B_Index)) {
		if (b->ino->type >= TypeBase) {
			u64 size = b->ino->md.file.size;
			if (size > ((loff_t)b->fileaddr * blksz) &&
			    size <= ((loff_t)(b->fileaddr + 1) * blksz))
				desc_start->block_bytes =
					__cpu_to_le32(size & (blksz-1));
			else
				desc_start->block_bytes =
					__cpu_to_le32(blksz);
		} else
			desc_start->block_bytes = __cpu_to_le32(blksz);
	}
}


int lafs_calc_cluster_csum(struct cluster_head *head)
{
	unsigned int oldcsum = head->checksum;
	unsigned long csum;

	head->checksum = 0;
	csum = crc32(0, (uint32_t *)head, __le16_to_cpu(head->Hlength));
	head->checksum = oldcsum;
	return __cpu_to_le32(csum);
}

void lafs_cluster_flush(struct lafs *fs, int cnum)
{
	char chead_buf[4096];
	struct cluster_head *ch;
	struct lafs_cluster *wc = &fs->wc[cnum];
	struct lafs_blk *b;
	int cluster_size;
	int i;
	loff_t head_addr[4];
	struct lafs_ino *current_inode = NULL;
	off_t current_block = NoBlock;
	struct descriptor *desc_start;
	struct group_head *head_start = NULL;
	struct lafs_device *dv;

	trace(1, "cluster flush\n");
	if (list_empty(&wc->blocks) &&
	    !(fs->checkpointing & CHECKPOINT_END)) {
		trace(1, "...skipped\n");
		return;
	}

	ch = (void*)chead_buf;

	wc->chead = chead_buf;
	wc->chead_size = sizeof(struct cluster_head);
	wc->chead_blocks = 1;
	memcpy(ch->idtag, "LaFSHead", 8);
	memcpy(ch->uuid, fs->uuid, 16);
	ch->seq = __cpu_to_le64(fs->wc[cnum].seq);
	fs->wc[cnum].seq++;

	cluster_size = seg_setsize(fs, &wc->seg,
				   seg_remainder(fs, &wc->seg) - wc->remaining);

	/* find, and step over, address header block(s) */
	for (i = 0; i < wc->chead_blocks ; i++)
		head_addr[i] = seg_next(fs, &wc->seg);

	ch->flags = 0;
	if (cnum == 0 && fs->checkpointing) {
		ch->flags = __cpu_to_le32(fs->checkpointing);
		if (fs->checkpointing & CHECKPOINT_END)
			fs->checkpointing = 0;
		else if (fs->checkpointing & CHECKPOINT_START) {
			fs->checkpoint_cluster = head_addr[0];
			fs->checkpointing &= ~CHECKPOINT_START;
		}
	}

	list_for_each_entry(b, &wc->blocks, leafs) {
		if (b->ino != current_inode) {
			/* need to create a new group_head */
			desc_start = NULL;
			if (head_start)
				cluster_closehead(wc, head_start);
			cluster_addhead(wc, b->ino, cnum, &head_start);
			current_inode = b->ino;
			current_block = NoBlock;
		}
		if (desc_start == NULL || b->fileaddr != current_block+1 ||
		    (b->flags & B_Index)) {
			cluster_adddesc(wc, b, &desc_start);
			current_block = b->fileaddr;
		} else
			current_block++;
		cluster_incdesc(wc, desc_start, b, fs->blocksize);

		lafs_allocated_block(b, seg_next(fs, &wc->seg));
	}
	if (head_start)
		cluster_closehead(wc, head_start);

	ch->Hlength = __cpu_to_le16(wc->chead_size);
	ch->Clength = __cpu_to_le16(cluster_size);
	ch->verify_type = VerifyNull;

	ch->next_addr = __cpu_to_le64(seg_addr(fs, &wc->seg));
	ch->prev_addr = __cpu_to_le64(wc->prev_addr);
	wc->prev_addr = head_addr[0];
	ch->this_addr = __cpu_to_le64(wc->prev_addr);
	ch->checksum = lafs_calc_cluster_csum(ch);

	dv = dev_by_num(fs, wc->seg.dev);

	for (i = 0; i < wc->chead_blocks; i++) {
		int dev;
		loff_t sect;
		virttophys(fs, head_addr[i], &dev, &sect);
		lseek64(dv->fd, sect, SEEK_SET);
		write(dv->fd, chead_buf+ i * fs->blocksize, fs->blocksize);
	}
	while (!list_empty(&wc->blocks)) {
		int dev;
		loff_t sect;

		b = list_first_entry(&wc->blocks, struct lafs_blk, leafs);
		list_del_init(&b->leafs);

		virttophys(fs, b->physaddr, &dev, &sect);
		lseek64(dv->fd, sect, SEEK_SET);
		b->flags &= ~B_Dirty;
		write(dv->fd, b->data, fs->blocksize);
		de_sched(b);
	}
}
