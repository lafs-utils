
#include <lafs/lafs.h>
#include <talloc.h>
#include <memory.h>
#include <lafs/list.h>
#include "internal.h"

int lafs_trace_level = 0;
static int tracing_set = 0;

struct lafs *lafs_alloc(void)
{
	struct lafs *fs = talloc(NULL, struct lafs);
	int i;

	if (!tracing_set) {
		char *e;
		tracing_set = 1;
		e = getenv("LAFS_TRACE");
		if (e)
			lafs_trace_level = atoi(e);
	}

	memset(fs, 0, sizeof(*fs));
	fs->statesize = 1024;

	INIT_LIST_HEAD(&fs->wc[0].blocks);
	INIT_LIST_HEAD(&fs->wc[1].blocks);
	INIT_LIST_HEAD(&fs->leafs);
	INIT_LIST_HEAD(&fs->account_leafs);
	INIT_LIST_HEAD(&fs->dirty_inodes);

	for (i=0; i < (1<<HASH_BITS); i++)
		INIT_LIST_HEAD(&fs->htable[i]);

	return fs;
}
