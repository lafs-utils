
/*
 * Allocate an unused inode number from the inode map.
 * We don't set it as in-use
 */

#include <lafs/lafs.h>
#include <memory.h>
#include "internal.h"

int lafs_imap_alloc(struct lafs_ino *imap)
{
	struct lafs *fs = imap->fs;
	struct lafs_dblk *db = NULL;

	u32 addr;
	u32 start = 0;
	u32 bit;

	while(1) {
		if (!db) {
			u32 bnum = lafs_find_next(imap, start);
			if (bnum < imap->md.inodemap.size)
				db = lafs_dblk(imap, bnum);

			else {
				/* need to allocate a new block past end of file */
				bnum = imap->md.inodemap.size;
				imap->md.inodemap.size = bnum+1;
				db = lafs_dblk(imap, bnum);
				lafs_sched_blk(&db->b);
				memset(db->b.data, 0xff, fs->blocksize);
				db->b.flags |= B_Valid;
			}
		}
		addr = db->b.fileaddr * fs->blocksize << 8;
		lafs_load_dblk(db);
		bit = find_first_bit(db->b.data, fs->blocksize);
		if (bit < fs->blocksize * 8) {
			lafs_sched_blk(&db->b);
			clear_c_bit(bit, (unsigned char *)db->b.data);
			if (*(u32*)(db->b.data) == 0
			    && db->b.data[fs->blocksize-1] == 0 &&
			    memcmp(db->b.data,
				   db->b.data+4, fs->blocksize-4) == 0) {
				/* block is completely zero, so we have to punch
				 * a hole
				 */
				db->b.flags &= ~B_Dirty;
				lafs_allocated_block(&db->b, 0);
				start = db->b.fileaddr + 1;
			}
			if (bit+addr < 16) {
				db=NULL;
				continue;
			}
			return bit + addr;
		}
		/* no 1 bits, so again, punch a hole */
		db->b.flags &= ~B_Dirty;
		lafs_allocated_block(&db->b, 0);
		start = db->b.fileaddr + 1;
		db = NULL;
	}
}
