
/* lafs_allocated_block.
 * This block has been allocated to this address (and will soon
 * be written there).
 * We need to update the accounting info.
 */

#include <lafs/lafs.h>
#include <stdlib.h>
#include <stdio.h>
#include "internal.h"

void lafs_allocated_block(struct lafs_blk *b, loff_t addr)
{
	struct lafs *fs = b->ino->fs;
	struct lafs_iblk *p;

	trace(1,"allocate %d/%lld %s to %lld\n", b->ino->inum,
	      (long long)b->fileaddr,
	      (b->flags & B_Index) ? "index":"data", (long long)addr);
	if (b->parent == NULL) {
		/* This is the root inode.  Its address goes
		 * directly in the 'state' block.
		 */
		if (b->flags & B_Index) abort();

		/* FIXME this might be a different snapshot */
		trace(1, "Root inode at %lld\n", (long long)addr);
		b->physaddr = addr;
		fs->ss.root_addr = addr;
		return;
	}

	p = b->parent;
	lafs_summary_update(b->ino, b->physaddr, addr,
			    !!(b->flags & B_Index));
	b->physaddr = addr;
	if (!(b->flags & B_Uninc)) {
		b->flags |= B_Uninc;
		b->chain = p->uninc;
		p->uninc = b;
		lafs_sched_blk(&p->b);
	}
}

