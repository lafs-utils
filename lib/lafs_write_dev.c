
#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <unistd.h>
#include <lafs/lafs.h>
#include <memory.h>
#include "internal.h"

static int size_bits(long long size)
{
	int bits = 0;
	while (size > 1) {
		size >>= 1;
		bits++;
	}
	return bits;
}

int lafs_write_dev(struct lafs_device *dev)
{
	char buf[LAFS_DEVBLK_SIZE];
	struct lafs_dev *pd = (void*)buf;
	struct lafs *fs = dev->fs;
	int i;
	uint32_t csum;

	memset(buf, 0, sizeof(buf));

	memcpy(pd->idtag, "LaFS-DeviceBlock", 16);
	pd->version = __cpu_to_le32(LAFS_DEV_VERS);

	memcpy(pd->uuid, fs->uuid, 16);
	dev->seq++;
	pd->seq = __cpu_to_le32(dev->seq);
	pd->ctime = lafs_encode_timeval(&dev->ctime);
	pd->start = __cpu_to_le64(dev->start);
	pd->size = __cpu_to_le64(dev->size);
	for (i=0; i<2; i++)
		pd->devaddr[i] = __cpu_to_le64(dev->devaddr[i]);
	for (i=0; i<4; i++)
		pd->stateaddr[i] = __cpu_to_le64(dev->stateaddr[i]);

	pd->statebits = size_bits(fs->statesize);
	pd->blockbits = size_bits(fs->blocksize);
	pd->width = __cpu_to_le16(dev->width);
	pd->stride = __cpu_to_le32(dev->stride);
	pd->segment_size = __cpu_to_le32(dev->segment_size);
	pd->segment_offset = __cpu_to_le32(dev->segment_offset);
	pd->segment_count = __cpu_to_le32(dev->segment_count);
	pd->usage_inum = __cpu_to_le32(dev->usage_inum);

	csum = crc32(0, (uint32_t*)buf, LAFS_DEVBLK_SIZE);
	pd->checksum = csum;

	for (i=0; i<2; i++) {
		lseek64(dev->fd, dev->devaddr[i], SEEK_SET);
		write(dev->fd, buf, LAFS_DEVBLK_SIZE);
	}

	return 0;
}
