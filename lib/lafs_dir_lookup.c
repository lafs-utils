#include <string.h>
#include <lafs/lafs.h>
#include "internal.h"

u32 lafs_dir_lookup(struct lafs_ino *dir, char *name, int len)
{
	/* lookup a name and return ino number, or 0 if not found */
	struct lafs_dblk *db;
	u32 hash;

	hash = lafs_hash_name(dir->md.file.seed, len, name);

	while (1) {
		u32 bnum;
		int found;
		u8 piece;
		struct dir_ent de;
		bnum = lafs_find_next(dir, hash+1);
		if (bnum == LAFS_NOBLOCK)
			bnum = 0;

		db = lafs_dblk(dir, bnum);
		if (!db ||
		    lafs_load_dblk(db))
			return 0;

		found = lafs_dir_find(db->b.data, dir->fs->blockbits-8,
				      dir->md.file.seed,
				      hash, &piece);
		if (!found)
			return 0;
		lafs_dir_extract(db->b.data, dir->fs->blockbits-8,
				 &de, piece, NULL);
		if (de.target &&
		    de.nlen == len &&
		    strncmp(de.name, name, len) == 0)
			return de.target;
		hash++;
	}
}
