#include <uuid/uuid.h>
#include <lafs/lafs.h>
#include <stdio.h>

void lafs_print_device(struct lafs_device *dev)
{
	char uuidstr[37];

	printf("Deviceblock:\n");
	printf(" IdTag      : %.16s\n", "LaFS-DeviceBlock");
	uuid_unparse(dev->uuid, uuidstr);
	printf(" UUID       : %s\n", uuidstr);
//	printf(" Checksum   : %08x\n", (unsigned int)le32_to_cpu(super->checksum));
	printf(" Seq        : %d\n", dev->seq);
	printf(" Creation   : %.24s\n", ctime(&dev->ctime.tv_sec));
	printf(" Start	    : %llu\n", (unsigned long long)dev->start);
	printf(" Size       : %llu\n", (unsigned long long)dev->size);
	printf(" Stride     : %lu\n", dev->stride);
	printf(" Width      : %lu\n", (unsigned long)dev->width);
	printf(" StateSize  : %d\n", dev->statesize);
	printf(" Devblk[0]  : %llu\n", (unsigned long long)dev->devaddr[0]);
	printf(" Devblk[1]  : %llu\n", (unsigned long long)dev->devaddr[1]);
	printf(" State[0]   : %llu\n", (unsigned long long)dev->stateaddr[0]);
	printf(" State[1]   : %llu\n", (unsigned long long)dev->stateaddr[1]);
	printf(" State[2]   : %llu\n", (unsigned long long)dev->stateaddr[2]);
	printf(" State[3]   : %llu\n", (unsigned long long)dev->stateaddr[3]);
	printf(" SegSize    : %lu\n", dev->segment_size);
	printf(" SegOffs    : %lu\n", dev->segment_offset);
	printf(" SegCount   : %lu\n", dev->segment_count);
	printf(" BlkBits    : %lu\n", (unsigned long)dev->blockbits);
	printf(" UsageInum  : %lu\n", (unsigned long)dev->usage_inum);
	printf("\n");
}
