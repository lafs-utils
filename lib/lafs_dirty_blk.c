
/*
 * mark a block for writeout eventually.
 */

#include <lafs/lafs.h>

#include <stdio.h>
#include "internal.h"

int lafs_dirty_blk(struct lafs_dblk *db)
{
	if (db->b.flags & B_Dirty)
		return 0;

	if (lafs_find_dblk(db) < 0)
		return -1;

	db->b.flags |= B_Dirty;
	if (db->b.flags & B_Sched)
		return 0;

	trace(1, "dirty %p\n", db);
	if (list_empty(&db->b.ino->dirty))
		list_move_tail(&db->b.ino->dirty_inodes,
			       &db->b.ino->fs->dirty_inodes);
	list_move_tail(&db->b.leafs, &db->b.ino->dirty);
	return 0;
}
