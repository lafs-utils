
#include <memory.h>
#include <talloc.h>
#include <lafs/lafs.h>

int lafs_include_dev(struct lafs *fs, struct lafs_device *dev, char **err)
{
	/* If fs not initialised, set up uuid/blocksize/statesize/etc
	 * If it is check the details match, and check this device
	 * doesn't overlap with any other device, and that state seq numbers
	 * are close enough.
	 */

	if (fs->blocksize == 0) {
		fs->blocksize = 1<<dev->blockbits;
		fs->blockbits = dev->blockbits;
		memcpy(fs->uuid, dev->uuid, 16);
		fs->statesize = dev->statesize;
		fs->seq = dev->state_seq;
		fs->devices = dev->devices;
	} else {
		if (fs->blocksize != (1<<dev->blockbits) ||
		    memcmp(fs->uuid, dev->uuid, 16) ||
		    fs->statesize != dev->statesize) {
			*err = "device is inconsistent with other devices in LaFS";
			return -1;
		}
		if (dev->state_seq > fs->seq) {
			fs->seq = dev->state_seq;
			fs->devices = dev->devices;
		}
	}

	if (fs->loaded_devs >= fs->devices) {
		*err = "already have enough devices for full LaFS";
		return -1;
	}
	fs->loaded_devs++;
	dev->next = fs->devs;
	fs->devs = dev;
	talloc_reparent(NULL, fs, dev);
	return 0;
}
