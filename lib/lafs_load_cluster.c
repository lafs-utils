#include <memory.h>
#include <lafs/lafs.h>
#include "internal.h"

/* Load a cluster-header given a virtual device address.
 * If the start looks OK, we load the full Hlength bytes in
 * malloced space, otherwise just the header-header
 *
 * Return:
 *   0 if all OK
 *   1 if truncated
 *  -1 if read error
 */

int lafs_load_cluster(struct lafs *fs, u64 addr, struct cluster_head **chp)
{
	char *buf = malloc(fs->blocksize);
	struct cluster_head *ch;
	int err;
	int len;
	int csum;
	int blocks;
	int i;

	err = lafs_read_virtual(fs, buf, addr);
	if (err) {
		free(buf);
		return -1;
	}
	ch = (void*)buf;
	*chp = ch;
	if (memcmp(ch->idtag, "LaFSHead", 8) != 0)
		return 1;
	if (memcmp(ch->uuid, fs->uuid, 16) != 0)
		return 1;
	if (__le64_to_cpu(ch->this_addr) != addr)
		return 1;
	len = __le16_to_cpu(ch->Hlength);
	if (len < sizeof(*ch))
		return 1;
	blocks = (len + fs->blocksize-1) >> fs->blockbits;
	if (blocks == 1)
		return 0;
	buf = realloc(buf, blocks << fs->blockbits);
	for (i=1; i<blocks; i++)
		if (lafs_read_virtual(fs, buf + i * fs->blocksize, addr+i)) {
			free(buf);
			return -1;
		}
	ch = (void*)buf;
	*chp = ch;
	csum = ch->checksum;
	lafs_calc_cluster_csum(ch);
	if (ch->checksum == csum)
		return 0;
	else
		return 1;
}
