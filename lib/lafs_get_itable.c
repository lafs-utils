#include <unistd.h>
#include <stdlib.h>
#include <lafs/lafs.h>
#include <memory.h>

/*
 * Get the primary inode table for a lafs filesystem.
 * If it doesn't exist we need to create it.
 */

struct lafs_ino *lafs_get_itable(struct lafs *fs)
{
	char *buf;
	struct lafs_dblk *blk;
	struct lafs_ino *ino;

	if (fs->ss.root)
		return fs->ss.root;

	buf = malloc(fs->blocksize);

	if (fs->ss.root_addr)
		lafs_read_virtual(fs, buf, fs->ss.root_addr);
	else
		lafs_inode_init(fs, buf, TypeInodeFile);

	ino = lafs_import_inode_buf(fs, buf, 0, NULL);

	blk = lafs_dblk(ino, 0);
	memcpy(blk->b.data, buf, fs->blocksize);
	free(buf);

	ino->dblock = blk;
	ino->filesys = ino;

	blk->b.flags |= B_Valid;
	blk->b.physaddr = fs->ss.root_addr;
	blk->my_inode = ino;
	if (blk->b.physaddr == 0)
		lafs_sched_blk(&blk->b);
	fs->ss.root = ino;
	return ino;
}
