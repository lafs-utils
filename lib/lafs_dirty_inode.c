/*
 * lafs_dirty_inode
 * This inode has been changed so we need to ensure it gets
 * written out.  In particular lafs_inode_fillblock needs to be
 * called at some stage.
 *
 */

#include <lafs/lafs.h>

void lafs_dirty_inode(struct lafs_ino *ino)
{
	ino->iflags |= I_Dirty;
	lafs_sched_blk(&ino->dblock->b);
}
