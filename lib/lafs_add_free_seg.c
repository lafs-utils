/*
 * Add a free segment to the freelist if there is room
 */

#include <lafs/lafs.h>

int lafs_add_free_seg(struct lafs *fs, int dev, loff_t seg)
{
	int next_tail = (fs->free_tail+1) & (128-1);

	if (next_tail == fs->free_head)
		/* no room */
		return 0;
	fs->freelist[fs->free_tail].dev = dev;
	fs->freelist[fs->free_tail].seg = seg;
	fs->free_tail = next_tail;
	return 1;
}
