
/* allocate a new segment at assign it to a lafs_cluster
 */

#include <lafs/lafs.h>

static void cluster_reset(struct lafs *fs, struct lafs_cluster *wc)
{
	wc->remaining = dev_by_num(fs, wc->seg.dev)->segment_size;
	wc->chead_blocks = 1;
	wc->remaining--;
	wc->chead_size = sizeof(struct cluster_head);
}

static void set_youth(struct lafs *fs, int dev, loff_t seg, int youth)
{
	struct lafs_device *dv;
	struct lafs_dblk *db;
	uint16_t *p;
	loff_t addr;

	dv = dev_by_num(fs, dev);
	addr = seg / (fs->blocksize >> YOUTH_SHIFT);

	db = lafs_dblk(dv->segsum, addr);
	lafs_load_dblk(db);
	p = (void*)db->b.data;
	p[seg % (fs->blocksize >> YOUTH_SHIFT)] = __cpu_to_le16(youth);
	lafs_sched_blk(&db->b);
}

int lafs_new_segment(struct lafs *fs, int cnum)
{
	int dev;
	loff_t seg;
	struct lafs_cluster *wc = &fs->wc[cnum];

	if (fs->free_head == fs->free_tail) {
		/* list is empty */
		lafs_find_free(fs);
		if (fs->free_head == fs->free_tail)
			return 0;
	}
	dev = fs->freelist[fs->free_head].dev;
	seg = fs->freelist[fs->free_head].seg;

	fs->free_head++;
	if (fs->free_head > 128)
		fs->free_head -= 128;

	wc->seg.dev = dev;
	wc->seg.num = seg;
	wc->seg.st_table = 0;
	wc->seg.st_row = 0;
	wc->seg.table = wc->seg.nxt_table = wc->seg.st_table;
	wc->seg.row = wc->seg.nxt_row = wc->seg.st_row;
	wc->seg.col = 0;

	cluster_reset(fs, wc);

	/* Need to set the youth for this segment */
	set_youth(fs, dev, seg, fs->youth_next++);
	return 1;
}
