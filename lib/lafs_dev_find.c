
#include <lafs/lafs.h>

struct lafs_device *
lafs_dev_find(struct lafs *fs, loff_t virt)
{
	struct lafs_device *dev;
	for (dev = fs->devs ; dev; dev = dev->next)
		if (virt >= dev->start &&
		    virt < dev->start + dev->size)
			return dev;
	abort();
	return NULL;
}
