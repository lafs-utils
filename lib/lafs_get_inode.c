
/* Find or load a given inode */
#include <lafs/lafs.h>

struct lafs_ino *lafs_get_inode(struct lafs_ino *fsys, int inum)
{
	struct lafs_dblk *db;

	db = lafs_dblk(fsys, inum);

	if (lafs_load_dblk(db))
		return NULL;

	if (db->my_inode == NULL)
		lafs_import_inode(db);
	return db->my_inode;
}

