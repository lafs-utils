#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64

#include <unistd.h>
#include <lafs/lafs.h>
#include <talloc.h>
#include <memory.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>

/* Add a new device to lafs.
 * This just sets up the internal data structures.
 * We at least need to lafs_write_dev() to write metadata out, and may want some
 * mount call to connect this to an active lafs.
 */

static int destroy(struct lafs_device *dev)
{
	close(dev->fd);
	return 0;
}

static int get_logical_block_size(int fd)
{
	struct stat stb;
	int sfd;
	int rv = -1;
	char path[60];
	char buf[20];
	fstat(fd, &stb);
	if ((stb.st_mode & S_IFMT) != S_IFBLK)
		return 512;
	sprintf(path, "/sys/dev/block/%d:%d/queue/logical_block_size",
		major(stb.st_rdev), minor(stb.st_rdev));
	sfd = open(path, O_RDONLY);
	if (sfd >= 0) {
		int n = read(sfd, buf, sizeof(buf));
		if (n >= 0)
			buf[n] = 0;
		close(sfd);
		rv = atoi(buf);
	}
	if (rv > 0)
		return rv;
	return 512;
}

struct lafs_device *lafs_add_device(struct lafs *fs, char *devname, int fd,
				    loff_t segblocks, loff_t strideblocks,
				    int width, int usage_inum)
{
	struct lafs_device *dev = talloc(fs, struct lafs_device);
	struct lafs_device *d2;
	unsigned long long size;
	int devblk;
	loff_t seg;

	memset(dev, 0, sizeof(*dev));
	dev->fd = fd;
	dev->seq = 1;
	dev->devnum = fs->loaded_devs++;
	dev->name = talloc_strdup(dev, devname);

	dev->width = width;
	dev->stride = strideblocks;
	dev->segment_size = segblocks;
	dev->usage_inum = usage_inum;
	dev->next = fs->devs;
	fs->devs = dev;
	dev->fs = fs;
	fs->devices++;

	if (dev->segment_size > fs->max_segment)
		fs->max_segment = dev->segment_size;

	if (dev->width * dev->stride <= dev->segment_size) {
		dev->tables_per_seg = dev->segment_size /
			dev->width / dev->stride;
		dev->rows_per_table = dev->stride;
		dev->segment_stride = dev->segment_size;
	} else {
		dev->tables_per_seg = 1;
		dev->rows_per_table = dev->segment_size / dev->width;
		dev->segment_stride = dev->rows_per_table;
	}
	gettimeofday(&dev->ctime, NULL);

	talloc_set_destructor(dev, destroy);

	/* now need to work out where the metadata goes and how much room
	 * is left for segments.
	 * Device block needs 1K and goes once at start and once at end.
	 * Start location is 1K in unless basic block size is larger
	 */
	if (ioctl(fd, BLKGETSIZE64, &size) < 0)
		size = lseek64(fd, 0, SEEK_END);
	devblk = get_logical_block_size(fd);
	if (devblk < LAFS_DEVBLK_SIZE)
		devblk = LAFS_DEVBLK_SIZE;

	size &= ~(unsigned long long)(devblk-1);


#if 0
	dev->devaddr[0] = devblk;
#else
	dev->devaddr[0] = 0;
#endif
	dev->devaddr[1] = size - devblk;
	/* State block has size set by 'fs->statesize'.
	 * We have two at the start of the device and two at the end.
	 * If stride*width < segment size we put them at multiples of width*stride
	 * If stride*width > segment size we put them at multiples of statesize
	 */
	/* FIXME just do the simple version for now */
	if (dev->width != 1 || dev->stride != 1)
		abort();
	if (devblk < fs->statesize)
		devblk = fs->statesize;
	dev->stateaddr[0] = 2 * devblk;
	dev->stateaddr[1] = 3 * devblk;
	dev->stateaddr[2] = size - 2*devblk;
	dev->stateaddr[3] = size - 3*devblk;

	/* segments need to align with width*stride too - later */
	dev->segment_offset = 4 * devblk;
	dev->segment_stride = dev->segment_size;
	dev->segment_count = (dev->stateaddr[3] - dev->segment_offset) / fs->blocksize / dev->segment_size;


	dev->tablesize = ((dev->segment_count + (fs->blocksize >> USAGE_SHIFT) + 1)
			  / (fs->blocksize >> USAGE_SHIFT));

	dev->size = dev->segment_count * dev->segment_size;

	/* Need to find a suitable offset */
	dev->start = 0;
	for (d2 = dev->next;  d2 ; d2 = d2->next) {
		if (dev->start < d2->start + d2->size &&
		    dev->start + dev->size > d2->start) {
			dev->start = d2->start + d2->size;
			/* start again from top */
			d2 = dev;
		}
	}

	for (seg = 0; seg < dev->segment_count; seg++)
		if (lafs_add_free_seg(fs, dev->devnum, seg) == 0)
			break;

	return dev;
}
