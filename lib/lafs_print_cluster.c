#include <stdio.h>
#include <lafs/lafs.h>
#include <uuid/uuid.h>
#include "internal.h"

static char *VerifyNames[] = {"Null", "Next", "Next2", "Sum",};
static char *FlagNames[] = {"Checkpoint","Start","End", NULL};

void lafs_print_cluster(struct cluster_head *head, int blocksize,
			int groups, int verbose)
{
	struct group_head *gh;
	struct miniblock *mb;
	struct descriptor *desc;
	u64 start;
	int i;
	long offset = 0;
	char uuidstr[37];

	if (verbose) {
		printf("ClusterHead:\n");
		printf(" IdTag     : %.8s\n", head->idtag);
		printf(" Flags     :");
		if (le32_to_cpu(head->flags)==0) printf(" -none-");
		else for (i=0; FlagNames[i]; i++)
			     if (le32_to_cpu(head->flags)&(1<<i))
				     printf(" %s", FlagNames[i]);
		printf("\n");
		uuid_unparse(head->uuid, uuidstr);
		printf(" UUID      : %s\n", uuidstr);
		printf(" Seq       : %llu\n", (unsigned long long)__le64_to_cpu(head->seq));
		printf(" Hlength   : %d\n", (int)__le16_to_cpu(head->Hlength));
		printf(" Clength   : %d\n", (int)__le16_to_cpu(head->Clength));
		printf(" Checksum  : %08x\n", (unsigned int)le32_to_cpu(head->checksum));
		printf(" Verify    : %s\n", VerifyNames[(int)__le16_to_cpu(head->verify_type)]);
		printf(" NEXT      : %llu\n", (unsigned long long)__le64_to_cpu(head->next_addr));
		printf(" THIS      : %llu\n", (unsigned long long)__le64_to_cpu(head->this_addr));
		printf(" PREV      : %llu\n", (unsigned long long)__le64_to_cpu(head->prev_addr));
	} else {
		char *sep="";
		printf("Cluster %llu(", (unsigned long long)__le64_to_cpu(head->seq));
		for (i=0; FlagNames[i]; i++)
			if (le32_to_cpu(head->flags)&(1<<i)) {
				printf("%s%s", sep, FlagNames[i]);
				sep = ",";
			}
		printf(") %llu < %llu > %llu\n",
		       (unsigned long long)__le64_to_cpu(head->prev_addr),
		       (unsigned long long)__le64_to_cpu(head->this_addr),
		       (unsigned long long)__le64_to_cpu(head->next_addr));
	}
	if (!groups)
		return;

	start = __le64_to_cpu(head->this_addr);
	offset = (__le16_to_cpu(head->Hlength)+blocksize-1)/blocksize;

	gh = head->groups;
	i = 0;
	while ( ((char*)gh - (char*)head) < __le16_to_cpu(head->Hlength)) {
		int j=0;
		if (verbose) {
			printf(" Group[%d]:\n", i);
			printf("  Inum    : %d\n", (int)le32_to_cpu(gh->inum));
			printf("  FSnum   : %d\n", (int)le32_to_cpu(gh->fsnum));
			printf("  Tstamp  : %llu\n", (unsigned long long)__le64_to_cpu(gh->timestamp));
			printf("  Flag    : %s\n", (__le16_to_cpu(gh->truncatenum_and_flag) & 0x8000) ? "old":"new");
			printf("  TRnum   : %d\n", (int)__le16_to_cpu(gh->truncatenum_and_flag) & 0x7fff);
			printf("  Size    : %d words\n", (int)__le16_to_cpu(gh->group_size_words));
		} else
			printf(" Inode: %d/%d\n", (int)le32_to_cpu(gh->fsnum),
			       (int)le32_to_cpu(gh->inum));
		desc = gh->u.desc;
		while (((char*)desc - (char*)gh) < __le16_to_cpu(gh->group_size_words)*4) {
			if (__le16_to_cpu(desc->block_bytes) <= DescMiniOffset ||
			    __le16_to_cpu(desc->block_bytes) == DescIndex ) {
				char *typ;
				if (__le16_to_cpu(desc->block_bytes) != DescIndex)
					typ = "Data";
				else
					typ = "Index";
				if (verbose) {
					printf("  Desc[%d]:\n", j);
					printf("   Block   : %u\n", (unsigned)le32_to_cpu(desc->block_num));
					printf("   Count   : %u\n", __le16_to_cpu(desc->block_cnt));
					printf("   Type    : %s\n", typ);
					printf("   Offset  : %ld (%llu)\n", offset,
					       (unsigned long long)start + offset);
				} else {
					if (__le16_to_cpu(desc->block_cnt) == 1)
						printf("  %s  : %u  (%llu)\n", typ,
						       (unsigned)le32_to_cpu(desc->block_num),
						       (unsigned long long)start + offset);
					else
						printf("  %s  : %u-%u  (%llu)\n", typ,
						       (unsigned)le32_to_cpu(desc->block_num),
						       (unsigned)le32_to_cpu(desc->block_num)
						       + __le16_to_cpu(desc->block_cnt) - 1,
						       (unsigned long long)start + offset);
				}
				offset += __le16_to_cpu(desc->block_cnt);
				desc++;
			} else {
				int len, k;
				mb = (struct miniblock *)desc;
				len = __le16_to_cpu(mb->length) - DescMiniOffset;
				if (verbose) {
					printf("  MiniBlock[%d]:\n", j);
					printf("   Block   : %u\n", (unsigned)le32_to_cpu(mb->block_num));
					printf("   Offset  : %u\n", __le16_to_cpu(mb->block_offset));
					printf("   length  : %u\n", len);
				} else
					printf("  Update: %u+%u - %u bytes\n",
					       (unsigned)le32_to_cpu(mb->block_num),
					       __le16_to_cpu(mb->block_offset), len);
				mb++;
				if (verbose) {
					printf("   Content : ");
					for (k=0; k<len; k++) {
						printf(" %02x", ((unsigned char*)mb)[k]);
						if (k > 100) {
							printf(" .....");
							break;
						}
					}
					printf("\n");
				}
				mb = (struct miniblock *)(((char*)mb) + ROUND_UP(len));
				desc = (struct descriptor *)mb;
			}
			j++;
		}
		gh = (struct group_head*)desc;
		i++;
	}
	if (verbose)
		printf(" Final Offset : %ld (next %llu)\n", offset, (unsigned long long)start+offset);
}
